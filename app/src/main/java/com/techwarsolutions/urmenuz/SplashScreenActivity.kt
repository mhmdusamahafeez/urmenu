package com.techwarsolutions.urmenuz

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.techwarsolutions.urmenuz.deliveryBoy.BoyDashboardActivity
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.waiter.WaiterDashboardActivity
import java.util.*

@SuppressLint("CustomSplashScreen")
class SplashScreenActivity : AppCompatActivity() {

    private var sharedPreference: SharedPreference? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        sharedPreference = SharedPreference(this@SplashScreenActivity)

/*
        val selectedLanguage = sharedPreference?.selectedLanguage
        if (selectedLanguage != null) {
            if (selectedLanguage.equals("English")) {
                setLocale("en")
            } else {
                setLocale("ar")
            }
        }else{
            setLocale("en")
        }*/
        gotoNextScreen()


    }


    private fun gotoNextScreen() {
        Handler().postDelayed({

            val intent = Intent(this@SplashScreenActivity, WaiterDashboardActivity::class.java)
            startActivity(intent)
            finish()

         /*   if (sharedPreference?.isPasswordCreated == "true") {
                if(sharedPreference?.isLogin == "true"){
                    val intent = Intent(this@SplashScreenActivity, WaiterDashboardActivity::class.java)
                    //val intent = Intent(this@SplashScreenActivity, BoyDashboardActivity::class.java)
                    startActivity(intent)
                    finish()
                }else{
                    val intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            } else {
                val intent = Intent(this@SplashScreenActivity, PreLoginActivity::class.java)
                startActivity(intent)
                finish()
            }*/

        }, 800)
    }

    private fun setLocale(language: String) {
        val resource = resources
        val metrics = resource.displayMetrics
        val configuration = resource.configuration
        val locale = Locale(language)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale)
        } else {
            configuration.locale = locale
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            applicationContext?.createConfigurationContext(configuration)
        resource.updateConfiguration(configuration, metrics)

        onConfigurationChanged(configuration)

        sharedPreference?.saveSelectedLanguage(language)

    }

}