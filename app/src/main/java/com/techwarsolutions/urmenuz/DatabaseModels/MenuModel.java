package com.techwarsolutions.urmenuz.DatabaseModels;




import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class MenuModel implements Serializable {

    @PrimaryKey(autoGenerate = true)
    int cartID;

    @ColumnInfo(name = "ItemName")
    private String itemName;

    @ColumnInfo(name = "CartUserID")
    private String cartUserID;

    @ColumnInfo(name = "ItemID")
    private String itemID;

    @ColumnInfo(name = "ItemPrice")
    private Double itemPrice;

    @ColumnInfo(name = "ItemQuantity")
    private Double itemQuantity;

    @ColumnInfo(name = "ItemImageURL")
    private String itemImageURL;

    @ColumnInfo(name = "ItemCategoryID")
    private String categoryID;

    @ColumnInfo(name = "ItemSize")
    private String itemSize;
    
    @ColumnInfo(name = "ItemTopping")
    private String itemTopping;



    public int getCartID() {
        return cartID;
    }

    public void setCartID(int cartID) {
        this.cartID = cartID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCartUserID() {
        return cartUserID;
    }

    public void setCartUserID(String cartUserID) {
        this.cartUserID = cartUserID;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public Double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Double getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(Double itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public String getItemImageURL() {
        return itemImageURL;
    }

    public void setItemImageURL(String itemImageURL) {
        this.itemImageURL = itemImageURL;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getItemSize() {
        return itemSize;
    }

    public void setItemSize(String itemSize) {
        this.itemSize = itemSize;
    }

    public String getItemTopping() {
        return itemTopping;
    }

    public void setItemTopping(String itemTopping) {
        this.itemTopping = itemTopping;
    }
}
