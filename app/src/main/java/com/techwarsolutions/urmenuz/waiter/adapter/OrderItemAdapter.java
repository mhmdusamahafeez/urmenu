package com.techwarsolutions.urmenuz.waiter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.waiter.model.OrderItemModel;

import java.util.ArrayList;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.OrderItemViewHolder> {

    Activity activity;
    ArrayList<OrderItemModel> categoriesArrayList;
    LayoutInflater inflater;

    public OrderItemAdapter(Activity activity, ArrayList<OrderItemModel> categoriesArrayList ) {
        this.activity = activity;
        this.categoriesArrayList = categoriesArrayList;
        this.inflater = activity.getLayoutInflater();
    }


    @NonNull
    @Override
    public OrderItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.order_details_list_items, null);
        return new OrderItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemViewHolder holder, @SuppressLint("RecyclerView") int position) {
        OrderItemModel OrderItemModel = categoriesArrayList.get(position);
        holder.orderItemQuantity.setText(OrderItemModel.getOrderItemQuantity());
        holder.orderItemName.setText(OrderItemModel.getOrderItemName());
        holder.orderItemPrice.setText(OrderItemModel.getOrderItemPrice());

    }

    @Override
    public int getItemCount() {
        return categoriesArrayList.size();
    }

    public class OrderItemViewHolder extends RecyclerView.ViewHolder {

        TextView orderItemQuantity;
        TextView orderItemName;
        TextView orderItemPrice;

        public OrderItemViewHolder(@NonNull View itemView) {
            super(itemView);


            orderItemName = itemView.findViewById(R.id.orderItemName);
            orderItemQuantity = itemView.findViewById(R.id.orderItemQuantity);
            orderItemPrice = itemView.findViewById(R.id.orderItemPrice);
        }
    }
}

