package com.techwarsolutions.urmenuz.waiter.model;

public class MyTablesModel {


    String tableID;
    String tableNo;
    String tableGuestsNo;
    String tableHostName;
    String tableOrderNo;
    String tableOrderTime;
    //String orderByCustomerStatus;
    String tableOccupiedStatus;
    String tableOccupiedType;
    String tableUnOccupiedType;
    String tableType;
    String orderPaymentStatus;
    String orderPaymentType;
    Boolean isSelectedTable = false;

    public MyTablesModel(String tableID, String tableNo, String tableGuestsNo, String tableHostName, String tableOrderNo, String tableOrderTime, String tableOccupiedStatus, String tableOccupiedType, String tableUnOccupiedType, String tableType, String orderPaymentStatus, String orderPaymentType) {
        this.tableID = tableID;
        this.tableNo = tableNo;
        this.tableGuestsNo = tableGuestsNo;
        this.tableHostName = tableHostName;
        this.tableOrderNo = tableOrderNo;
        this.tableOrderTime = tableOrderTime;
        this.tableOccupiedStatus = tableOccupiedStatus;
        this.tableOccupiedType = tableOccupiedType;
        this.tableUnOccupiedType = tableUnOccupiedType;
        this.tableType = tableType;
        this.orderPaymentStatus = orderPaymentStatus;
        this.orderPaymentType = orderPaymentType;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getTableID() {
        return tableID;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    public String getTableNo() {
        return tableNo;
    }

    public void setTableNo(String tableNo) {
        this.tableNo = tableNo;
    }

    public String getTableGuestsNo() {
        return tableGuestsNo;
    }

    public void setTableGuestsNo(String tableGuestsNo) {
        this.tableGuestsNo = tableGuestsNo;
    }

    public String getTableHostName() {
        return tableHostName;
    }

    public void setTableHostName(String tableHostName) {
        this.tableHostName = tableHostName;
    }

    public String getTableOrderNo() {
        return tableOrderNo;
    }

    public void setTableOrderNo(String tableOrderNo) {
        this.tableOrderNo = tableOrderNo;
    }

    public String getTableOrderTime() {
        return tableOrderTime;
    }

    public void setTableOrderTime(String tableOrderTime) {
        this.tableOrderTime = tableOrderTime;
    }

    public String getTableOccupiedStatus() {
        return tableOccupiedStatus;
    }

    public void setTableOccupiedStatus(String tableOccupiedStatus) {
        this.tableOccupiedStatus = tableOccupiedStatus;
    }

    public String getTableOccupiedType() {
        return tableOccupiedType;
    }

    public void setTableOccupiedType(String tableOccupiedType) {
        this.tableOccupiedType = tableOccupiedType;
    }

    public String getTableUnOccupiedType() {
        return tableUnOccupiedType;
    }

    public void setTableUnOccupiedType(String tableUnOccupiedType) {
        this.tableUnOccupiedType = tableUnOccupiedType;
    }

    public String getOrderPaymentStatus() {
        return orderPaymentStatus;
    }

    public void setOrderPaymentStatus(String orderPaymentStatus) {
        this.orderPaymentStatus = orderPaymentStatus;
    }

    public String getOrderPaymentType() {
        return orderPaymentType;
    }

    public void setOrderPaymentType(String orderPaymentType) {
        this.orderPaymentType = orderPaymentType;
    }

    public Boolean getSelectedTable() {
        return isSelectedTable;
    }

    public void setSelectedTable(Boolean selectedTable) {
        isSelectedTable = selectedTable;
    }
}
