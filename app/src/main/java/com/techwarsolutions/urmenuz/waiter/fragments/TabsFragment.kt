package com.techwarsolutions.urmenuz.waiter.fragments

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.BlurDialog
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import com.techwarsolutions.urmenuz.waiter.adapter.TabItemAdapter
import com.techwarsolutions.urmenuz.waiter.interfaces.UrTabInterfaces
import com.techwarsolutions.urmenuz.waiter.model.MyTablesModel
import com.techwarsolutions.urmenuz.waiter.model.TabItemsModel
import kotlinx.android.synthetic.main.fragment_tabs.*


class TabsFragment : Fragment(), UrTabInterfaces.OnTabClickListener {

    private var sharedPreference: SharedPreference? = null
    var tableModel: MyTablesModel? = null
    var tabItemsModel: TabItemsModel? = null
    private var tabItemArrayList: ArrayList<TabItemsModel>? = null
    var swipeListener: SwipeListener? = null
    var blurDialog: BlurDialog? = null


    private var tabItemList = arrayOf(
        "Vegetable Corn Soup",
        "Chicken Corn Soup",
    )
    private var tabItemQuantityList = arrayOf("3", "4")
    private var tabItemPriceList = arrayOf("23.00", "24.00")

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tabs, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeListener = SwipeListener(swipeTab, requireActivity())
        swipeListener = SwipeListener(swipeTab2, requireActivity())

        sharedPreference = SharedPreference(activity)
        if (sharedPreference?.userImage != null && (sharedPreference?.userImage)?.isNotEmpty()!!) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(btnOpenWaiterSettings!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }

        val bundle = arguments
        if (bundle != null) {
            tableModel =
                Gson().fromJson(bundle.getString("tableInformation"), MyTablesModel::class.java)

            if (tableModel?.tableType.equals("Hosted")) {
                guestItemsLayout?.visibility = View.VISIBLE
            } else {
                guestItemsLayout?.visibility = View.GONE
            }
            tblNumber?.setText(tableModel?.tableNo)


            // Open Confirm Order Dialog
            btnConfirmOrder?.setOnClickListener {
                try {
                    confirmOrderDialog?.visibility = View.VISIBLE
                    confirmOrderDialog.setBackgroundDrawable(BitmapDrawable(resources,
                        blur(requireActivity(), captureScreenShot(swipeTab)!!)))
                } catch (e: Exception) {
                    Log.e("ConfirmDialog", e.toString())
                }
            }

            // Dialog Button to proceeds further
            // When the waiter confirm orders he must have move back to same table details page of which
            // he he is placing order. when pressing back moves back to home page not back to menu page or description page
            // Shortly,
            // Confirm Order for TBl#5 ->  Tbl#5 TableDetailsFragment -> when back pressed moves -> WaiterHomeFragment

            // Not
            //Confirm Order for TBl#5 -> Tbl#5 TableDetailsFragment -> when back pressed moves ->
            // RestaurantMenuFragment -> ItemDescriptionFragment -> etc

            btnDialogConfirmOrder?.setOnClickListener {
                sharedPreference?.saveAddToTab("false")
                confirmOrderDialog?.visibility = View.GONE

//                if (activity?.supportFragmentManager?.backStackEntryCount!! > 0) {
//                    activity?.supportFragmentManager?.popBackStack(2,
//                        FragmentManager.POP_BACK_STACK_INCLUSIVE)
//                    activity?.supportFragmentManager?.popBackStack()

                    val bundle1 = Bundle()
                    bundle1.putString("tableInformation", Gson().toJson(tableModel))
                    val nextFrag = TableDetailsFragment()
                    nextFrag.arguments = bundle1
                    setFragment(nextFrag)
//                }
            }
            // click outside dialog to dismiss dialog
            confirmOrderDialog?.setOnClickListener {
                confirmOrderDialog?.visibility = View.GONE
            }

        }


        btnAddMoreItems?.setOnClickListener {
            setFragment(RestaurantMenuFragment())
        }

        btnOpenWaiterSettings?.setOnClickListener {
            setFragment(WaiterSettingsFragment())
        }

        btnBackurTab?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }

        tabItemArrayList = java.util.ArrayList<TabItemsModel>()
        for (i in tabItemList.indices) {
            val model = TabItemsModel(tabItemList.get(i),
                tabItemQuantityList.get(i),
                tabItemPriceList.get(i))

            tabItemArrayList!!.add(model)
        }
        val adapter = TabItemAdapter(activity, tabItemArrayList, this)
        rvTabItems.adapter = adapter

        /*Guest1*/
        tabItemArrayList = java.util.ArrayList<TabItemsModel>()
        for (i in tabItemList.indices) {
            val model = TabItemsModel(tabItemList.get(i),
                tabItemQuantityList.get(i),
                tabItemPriceList.get(i))

            tabItemArrayList!!.add(model)
        }
        val guestAdapter = TabItemAdapter(activity, tabItemArrayList, this)
        rvGuest1TabItems.adapter = guestAdapter

        closeGuest1Items?.setOnClickListener {
            rvGuest1TabItems?.visibility = View.GONE
            btnAddGuest1MoreItems?.visibility = View.GONE
            closeGuest1Items?.visibility = View.GONE
            openGuest1Items?.visibility = View.VISIBLE
        }
        removeGuestDialog?.setOnClickListener {
            removeGuestDialog.visibility = View.GONE
        }
        btnRemoveGuest1Item?.setOnClickListener {
            try {
                removeGuestDialog.setBackgroundDrawable(BitmapDrawable(resources,
                    blur(requireActivity(), captureScreenShot(swipeTab)!!)))
                removeGuestDialog.visibility = View.VISIBLE

            } catch (e: Exception) {
                Log.e("RemoveDialog", e.toString())

            }
        }

        openGuest1Items?.setOnClickListener {
            rvGuest1TabItems?.visibility = View.VISIBLE
            btnAddGuest1MoreItems?.visibility = View.VISIBLE
            closeGuest1Items?.visibility = View.VISIBLE
            openGuest1Items?.visibility = View.GONE
        }
        /*Guest 1*/


        btnCashPayment?.setOnClickListener {
            cashPayRB.isChecked = true;
            creditPayRB.isChecked = false;
        }
        btnCreditPayment?.setOnClickListener {
            cashPayRB.isChecked = false;
            creditPayRB.isChecked = true;

        }

        /* remove items from cart*/
        removeItemDialog?.setOnClickListener {
            removeItemDialog?.visibility = View.GONE
        }
        btnRemoveItemNo?.setOnClickListener {
            removeItemDialog?.visibility = View.GONE
        }
        btnRemoveItemYes?.setOnClickListener {
            removeItemDialog?.visibility = View.GONE
        }

    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()
    }

    override fun addQuantityClick(view: View?, itemsModel: TabItemsModel?, position: Int) {
        TODO("Not yet implemented")
    }

    override fun removeQuantityClick(view: View?, itemsModel: TabItemsModel?, position: Int) {
        TODO("Not yet implemented")
    }


    override fun removeItemClick(view: View?, itemsModel: TabItemsModel?, position: Int) {
        try {
            removeItemDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(swipeTab)!!)))
            removeItemDialog.visibility = View.VISIBLE

        } catch (e: Exception) {
            Log.e("RemoveDialog", e.toString())

        }
    }

    fun captureScreenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_4444)
        val canvas = Canvas(bitmap)
        val backgroundDrawable = view.background
        if (backgroundDrawable != null) {
            backgroundDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.parseColor("#80000000"))
        }
        view.draw(canvas)
        return bitmap
    }

    fun blur(context: Context?, image: Bitmap): Bitmap? {
        val BITMAP_SCALE = 0.4f
        val BLUR_RADIUS = 9.5f
        val width = Math.round(image.width * BITMAP_SCALE)
        val height = Math.round(image.height * BITMAP_SCALE)
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs = RenderScript.create(context)
        val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(BLUR_RADIUS)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }
}