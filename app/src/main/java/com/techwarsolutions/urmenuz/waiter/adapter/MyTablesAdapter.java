package com.techwarsolutions.urmenuz.waiter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.waiter.interfaces.TableInterfaces;
import com.techwarsolutions.urmenuz.waiter.model.MyTablesModel;

import java.util.ArrayList;


public class MyTablesAdapter extends RecyclerView.Adapter<MyTablesAdapter.MyTablesViewHolder> {

    Activity activity;
    LayoutInflater inflater;
    ArrayList<MyTablesModel> tablesModelArrayList;
    TableInterfaces.OnTableClickListener onTableClickListener;
    String state = "false";
    String unOccupy = "false";
    String visibility = "GONE";

    public MyTablesAdapter(Activity activity, ArrayList<MyTablesModel> tablesModelArrayList, TableInterfaces.OnTableClickListener onTableClickListener) {
        this.activity = activity;
        this.tablesModelArrayList = tablesModelArrayList;
        this.inflater = activity.getLayoutInflater();
        this.onTableClickListener = onTableClickListener;
    }

    @NonNull
    @Override
    public MyTablesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_waiter_table, null);
        return new MyTablesAdapter.MyTablesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyTablesViewHolder holder, @SuppressLint("RecyclerView") int position) {
        MyTablesModel model = tablesModelArrayList.get(position);
        holder.txtTableNo.setText("Tbl " + model.getTableNo());
        holder.txtPaymentStatus.setText(model.getOrderPaymentStatus());
        holder.txtNoOfGuest.setText(model.getTableGuestsNo());


        if (visibility.equals("VISIBLE")) {
            if(model.getTableOccupiedStatus().equalsIgnoreCase("Unoccupied")){
                if(model.getTableUnOccupiedType().equalsIgnoreCase("Available")){
                    holder.tableUnChecked.setVisibility(View.INVISIBLE);
                }else {
                    holder.tableUnChecked.setVisibility(View.VISIBLE);
                }

            }else {
                holder.tableUnChecked.setVisibility(View.VISIBLE);

            }

        } else {
            model.setSelectedTable(false);
            holder.tableUnChecked.setVisibility(View.INVISIBLE);
            holder.tableChecked.setVisibility(View.INVISIBLE);
        }
        if (model.getSelectedTable()) {
            holder.tableChecked.setVisibility(View.VISIBLE);
        } else {
            holder.tableChecked.setVisibility(View.GONE);
        }

        if (unOccupy.equals("unOccupy")) {
            holder.occupyLayoutStatus.setBackground(activity.getResources().getDrawable(R.drawable.unoccupy_avaliable_outline));
            holder.paymentLayout.setVisibility(View.INVISIBLE);
            holder.orderPlacedImg.setVisibility(View.INVISIBLE);
        } else {


            if (model.getTableOccupiedStatus().equalsIgnoreCase("Occupied")) {
                if (model.getTableOccupiedType().equalsIgnoreCase("Confirmed Paid")) {
                    holder.occupyLayoutStatus.setBackground(activity.getResources().getDrawable(R.drawable.confirmed_paid_outline));
                    holder.orderPlacedImg.setVisibility(View.VISIBLE);
                    holder.paymentLayout.setVisibility(View.VISIBLE);

                } else if (model.getTableOccupiedType().equalsIgnoreCase("Confirmed Unpaid")) {
                    holder.occupyLayoutStatus.setBackground(activity.getResources().getDrawable(R.drawable.confirmed_unpaid_outline));
                    holder.orderPlacedImg.setVisibility(View.VISIBLE);
                    holder.paymentLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.occupyLayoutStatus.setBackground(activity.getResources().getDrawable(R.drawable.occupied_no_order));
                    holder.orderPlacedImg.setVisibility(View.INVISIBLE);
                    holder.paymentLayout.setVisibility(View.INVISIBLE);
                }
            } else {

                if (model.getTableUnOccupiedType().equalsIgnoreCase("Reserved")) {
                    holder.occupyLayoutStatus.setBackground(activity.getResources().getDrawable(R.drawable.unoccupy_reserved_outline));
                    holder.orderPlacedImg.setVisibility(View.INVISIBLE);
                    holder.paymentLayout.setVisibility(View.INVISIBLE);

                } else if (model.getTableUnOccupiedType().equalsIgnoreCase("Available")) {
                    holder.occupyLayoutStatus.setBackground(activity.getResources().getDrawable(R.drawable.unoccupy_avaliable_outline));
                    holder.orderPlacedImg.setVisibility(View.INVISIBLE);
                    holder.paymentLayout.setVisibility(View.INVISIBLE);
                }

            }

        }
        if (model.getOrderPaymentStatus().equals("Paid")) {
            holder.waiterCashIcon.setVisibility(View.VISIBLE);
            holder.txtPaymentStatus.setText(model.getOrderPaymentType());

            if (model.getOrderPaymentType().equals("Cash")) {
                holder.waiterCashIcon.setImageResource(R.drawable.money);
            } else {
                holder.waiterCashIcon.setImageResource(R.drawable.ic_credit_method);
            }

        } else {
            holder.waiterCashIcon.setVisibility(View.GONE);
        }

        holder.tableDetailLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (model.getSelectedTable()) {
                    holder.tableChecked.setVisibility(View.GONE);
                    model.setSelectedTable(false);
                    notifyDataSetChanged();
                    if (getSelectedOrders().size() == 0) {
                        onTableClickListener.onSelectTableAction(false);
                    }
                } else {
                    if (state.equals("true")) {
                        model.setSelectedTable(true);
                        notifyDataSetChanged();
                        holder.tableChecked.setVisibility(View.VISIBLE);
                        onTableClickListener.onSelectTableAction(true);
                    } else {
                        onTableClickListener.OnTableClick(view, model, position);
                    }
                }

            }
        });

    }

    public ArrayList<MyTablesModel> getSelectedOrders() {
        ArrayList<MyTablesModel> selectedTables = new ArrayList<>();
        for (MyTablesModel tablesModel : tablesModelArrayList) {
            if (tablesModel.getSelectedTable()) {
                selectedTables.add(tablesModel);
            }
        }
        return selectedTables;
    }

    public void setViewState(String state, String visibility, String unOccupy) {
        this.unOccupy = unOccupy;
        this.state = state;
        this.visibility = visibility;
    }


    @Override
    public int getItemCount() {
        return tablesModelArrayList.size();
    }

    static class MyTablesViewHolder extends RecyclerView.ViewHolder {
        TextView txtTableNo;
        TextView txtPaymentStatus;
        TextView txtNoOfGuest;
        ImageView orderPlacedImg;
        ImageView waiterCashIcon;
        ImageView tableUnChecked;
        ImageView tableChecked;
        FrameLayout occupyLayoutStatus;
        CardView tableDetailLayout;
        LinearLayout paymentLayout;

        public MyTablesViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTableNo = itemView.findViewById(R.id.txtTableNo);
            txtPaymentStatus = itemView.findViewById(R.id.txtPaymentStatus);
            txtNoOfGuest = itemView.findViewById(R.id.txtNoOfGuest);
            tableUnChecked = itemView.findViewById(R.id.tableUnChecked);
            tableChecked = itemView.findViewById(R.id.tableChecked);
            orderPlacedImg = itemView.findViewById(R.id.orderPlacedImg);
            waiterCashIcon = itemView.findViewById(R.id.waiterCashIcon);
            occupyLayoutStatus = itemView.findViewById(R.id.occupyLayoutStatus);
            tableDetailLayout = itemView.findViewById(R.id.tableDetailLayout);
            paymentLayout = itemView.findViewById(R.id.paymentLayout);
        }
    }

}
