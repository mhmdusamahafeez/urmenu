package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import com.techwarsolutions.urmenuz.waiter.adapter.PreOrderHeaderAdapter
import com.techwarsolutions.urmenuz.waiter.model.PreviousOrderHeaderModel
import com.techwarsolutions.urmenuz.waiter.model.PreviousOrderModel
import kotlinx.android.synthetic.main.fragment_previous_orders.*

class PreviousOrdersFragment : Fragment() {

    var headerOrderArrayList: ArrayList<PreviousOrderHeaderModel>? = null
    var orderItemArrayList: ArrayList<PreviousOrderModel>? = null
    var swipeListener: SwipeListener? = null
    private var sharedPreference: SharedPreference? = null


    private var orderHeaderList = arrayOf("Today", "3/8/2022", "3/7/2022", "3/6/2022")
    private var orderNoList = arrayOf("537", "538")
    private var paymentMethodList = arrayOf("Cash", "Credit")
    private var orderAmountList = arrayOf(20.00, 30.00)
    private var dateTimeList = arrayOf(
        "Placed at 8:30 pm | Feb 28,2022",
        "Placed at 8:40 pm | Feb 27,2022"
    )


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_previous_orders, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeListener = SwipeListener(swipePreOrders, requireActivity())
        sharedPreference = SharedPreference(activity)

        if (sharedPreference?.userImage != null && (sharedPreference?.userImage)?.isNotEmpty()!!) {
            try {
                Glide.with(requireActivity())
                    .load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user)
                    .into(btnOpenPrWaiterSetting!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }

        btnBackPreOrder?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
        btnOpenPrWaiterSetting?.setOnClickListener {
            setFragment(WaiterSettingsFragment())
        }

        headerOrderArrayList = java.util.ArrayList()
        for (MainCategory in orderHeaderList) {
            orderItemArrayList = java.util.ArrayList()
            for (i in orderNoList.indices) {
                val model = PreviousOrderModel(
                    orderNoList[i], paymentMethodList[i], dateTimeList[i], orderAmountList[i]
                )
                orderItemArrayList!!.add(model)
            }
            val mainTopModel =
                PreviousOrderHeaderModel(MainCategory, orderItemArrayList)
            headerOrderArrayList!!.add(mainTopModel)
        }

        val adapter = PreOrderHeaderAdapter(activity, headerOrderArrayList)
        val mLayoutManager = LinearLayoutManager(activity)
        rvPreviousOrder.layoutManager = mLayoutManager
        rvPreviousOrder!!.adapter = adapter


    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(
                0,
                0,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)
            ?.commit()
    }
}