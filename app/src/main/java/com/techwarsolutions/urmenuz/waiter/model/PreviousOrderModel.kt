package com.techwarsolutions.urmenuz.waiter.model

class PreviousOrderModel {
    var txtPreOrderID: String? = null
    var txtPreOrderNo: String? = null
    var txtPaymentMethod: String? = null
    var txtPreOrderDateTime: String? = null
    var txtPreOrderAmount: Double? = 0.0

    constructor(
        txtPreOrderNo: String?,
        txtPaymentMethod: String?,
        txtPreOrderDateTime: String?,
        txtPreOrderAmount: Double?
    ) {
        this.txtPreOrderNo = txtPreOrderNo
        this.txtPaymentMethod = txtPaymentMethod
        this.txtPreOrderDateTime = txtPreOrderDateTime
        this.txtPreOrderAmount = txtPreOrderAmount
    }
}