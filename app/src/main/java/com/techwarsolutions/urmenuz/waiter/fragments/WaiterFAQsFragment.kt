package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import kotlinx.android.synthetic.main.fragment_tems_and_conditions.*
import kotlinx.android.synthetic.main.fragment_waiter_f_a_qs.*



class WaiterFAQsFragment : Fragment() {


    private var sharedPreference: SharedPreference? = null
    private var swipeListener: SwipeListener? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_waiter_f_a_qs, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedPreference = SharedPreference(activity)
        swipeListener = SwipeListener(swipeWaiterFAQ, requireActivity())

        if (sharedPreference?.userImage != null) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(btnOpenUserSettings!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }
        btnBacksWaiterFAQ?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
        btnOpenWaiterSetting?.setOnClickListener {
            setFragment(WaiterSettingsFragment())
        }
    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()
    }
}