package com.techwarsolutions.urmenuz.waiter.interfaces;

import android.view.View;

import com.techwarsolutions.urmenuz.waiter.model.CategoryHeaderModel;
import com.techwarsolutions.urmenuz.waiter.model.CategoryItemsModel;

public class MenuItemInterface {

    public interface OnMenuItemClickListener {
        void OnMenuItemClick(View view, CategoryItemsModel itemsModel, int position);
       // void onSelectMenuItemAction(int changePosition);


    }

}
