package com.techwarsolutions.urmenuz.waiter.interfaces;

import android.view.View;

import com.techwarsolutions.urmenuz.waiter.model.TabItemsModel;

public class UrTabInterfaces {

    public interface OnTabClickListener {
        void addQuantityClick(View view, TabItemsModel itemsModel, int position);
        void removeQuantityClick(View view, TabItemsModel itemsModel, int position);
        void removeItemClick(View view, TabItemsModel itemsModel, int position);


    }
}
