package com.techwarsolutions.urmenuz.waiter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.waiter.fragments.PreviousOrderSummaryFragment;
import com.techwarsolutions.urmenuz.waiter.interfaces.PreOrderInterface;
import com.techwarsolutions.urmenuz.waiter.model.PreviousOrderHeaderModel;
import com.techwarsolutions.urmenuz.waiter.model.PreviousOrderModel;

import java.util.ArrayList;

public class PreOrderHeaderAdapter extends RecyclerView.Adapter<PreOrderHeaderAdapter.MainHeaderViewHolder> implements PreOrderInterface.OnPreOrderClickListener {

    Activity activity;
    ArrayList<PreviousOrderHeaderModel> categoryHeaderModelArrayList;
    LayoutInflater inflater;
    PreOrderInterface.OnPreOrderClickListener orderClickListener;


    public PreOrderHeaderAdapter(Activity activity, ArrayList<PreviousOrderHeaderModel> categoryHeaderModelArrayList) {
        this.activity = activity;
        this.categoryHeaderModelArrayList = categoryHeaderModelArrayList;
        this.inflater = activity.getLayoutInflater();
    }


    @NonNull
    @Override
    public PreOrderHeaderAdapter.MainHeaderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.pre_order_header_list_items, null);
        return new PreOrderHeaderAdapter.MainHeaderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PreOrderHeaderAdapter.MainHeaderViewHolder holder, @SuppressLint("RecyclerView") int position) {
        PreviousOrderHeaderModel categoryHeaderModel = categoryHeaderModelArrayList.get(position);
        holder.txtPreOrderHeaderName.setText(categoryHeaderModel.getPreOrderHeaderName());
        PreviousOrderAdapter adapter = new PreviousOrderAdapter(activity, categoryHeaderModel.getOrderModelArrayList(), this);
        LinearLayoutManager manager = new LinearLayoutManager(activity);
        holder.rv_preOrder_items.setLayoutManager(manager);
        holder.rv_preOrder_items.setAdapter(adapter);


    }

    @Override
    public int getItemCount() {
        return categoryHeaderModelArrayList.size();
    }

    @Override
    public void OnPreOrderClick(View view, PreviousOrderModel orderModel, int position) {
        AppCompatActivity activity = (AppCompatActivity) view.getContext();
        Bundle bundle = new Bundle();
        bundle.putString("preOrderInformation", new Gson().toJson(orderModel));

        Fragment myFragment = new PreviousOrderSummaryFragment();
        myFragment.setArguments(bundle);

        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.waiterContainer, myFragment)
                .setCustomAnimations(
                        R.anim.enter_from_right,
                        R.anim.exit_to_left,
                        R.anim.enter_from_left,
                        R.anim.exit_to_right
                ).addToBackStack(myFragment.getClass().getName()).commit();

    }


    public class MainHeaderViewHolder extends RecyclerView.ViewHolder {
        TextView txtPreOrderHeaderName;
        RecyclerView rv_preOrder_items;
        LinearLayout headerLayout;

        public MainHeaderViewHolder(@NonNull View itemView) {
            super(itemView);

            txtPreOrderHeaderName = itemView.findViewById(R.id.txtPreOrderHeaderName);
            //headerLayout = itemView.findViewById(R.id.headerLayout);
            rv_preOrder_items = itemView.findViewById(R.id.rv_preOrder_items);
        }
    }


}
