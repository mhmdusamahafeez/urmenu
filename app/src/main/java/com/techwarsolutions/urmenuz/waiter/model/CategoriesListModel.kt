package com.techwarsolutions.urmenuz.waiter.model

class CategoriesListModel {
    var categoryID: String? = null
    var categoryIcon = 0
    var txtCategoryName: String? = null
    var isSelected = false

    constructor(categoryIcon: Int, txtCategoryName: String?) {
        this.categoryIcon = categoryIcon
        this.txtCategoryName = txtCategoryName
    }
}