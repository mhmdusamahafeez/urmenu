package com.techwarsolutions.urmenuz.waiter.model;

public class SuggestedItemModel {

    int suggestedItemImage;
    String txtSuggestedItemPrice;
    Boolean isSelectedItem = false;

    public SuggestedItemModel(int suggestedItemImage, String txtSuggestedItemPrice) {
        this.suggestedItemImage = suggestedItemImage;
        this.txtSuggestedItemPrice = txtSuggestedItemPrice;
    }

    public int getSuggestedItemImage() {
        return suggestedItemImage;
    }

    public void setSuggestedItemImage(int suggestedItemImage) {
        this.suggestedItemImage = suggestedItemImage;
    }

    public String getTxtSuggestedItemPrice() {
        return txtSuggestedItemPrice;
    }

    public void setTxtSuggestedItemPrice(String txtSuggestedItemPrice) {
        this.txtSuggestedItemPrice = txtSuggestedItemPrice;
    }

    public Boolean getSelectedItem() {
        return isSelectedItem;
    }

    public void setSelectedItem(Boolean selectedItem) {
        isSelectedItem = selectedItem;
    }
}
