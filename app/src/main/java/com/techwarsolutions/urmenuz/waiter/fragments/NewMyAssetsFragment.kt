package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.ViewAdapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_new_my_assets.*


class NewMyAssetsFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_my_assets, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnBackMyEarn?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }

        btnOpenWaiterSetting?.setOnClickListener {
            setFragment(WaiterSettingsFragment())
        }

        try {
            val viewPagerAdapter = ViewPagerAdapter(activity?.supportFragmentManager)
            viewPagerWaiter.adapter = viewPagerAdapter

            // viewPagerAdapter.addTabs(PreviousOrdersFragment(), "Previous Orders")
            // viewPagerAdapter.addTabs(TipsReceivedFragment(), "Tips Received")
            viewPagerAdapter.notifyDataSetChanged()

            tabLayoutWaiter.setupWithViewPager(viewPagerWaiter)
        } catch (e: Exception) {
            Log.e("AssetsIssue", e.toString())
        }


    }


    private fun setFragment(fragment: Fragment?) {
        try {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.waiterContainer, fragment!!)
                ?.setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right
                )
                ?.addToBackStack(fragment.javaClass.canonicalName)
                ?.commit()

        } catch (e: Exception) {
            Log.e("TabLayoutIssue", e.toString())
        }
    }
}