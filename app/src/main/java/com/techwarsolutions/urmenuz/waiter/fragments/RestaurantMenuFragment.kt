package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import com.techwarsolutions.urmenuz.waiter.adapter.CategoriesListAdapter
import com.techwarsolutions.urmenuz.waiter.adapter.CategoryHeaderAdapter
import com.techwarsolutions.urmenuz.waiter.adapter.CategoryItemsAdapter
import com.techwarsolutions.urmenuz.waiter.interfaces.MenuInterface
import com.techwarsolutions.urmenuz.waiter.model.CategoriesListModel
import com.techwarsolutions.urmenuz.waiter.model.CategoryHeaderModel
import com.techwarsolutions.urmenuz.waiter.model.CategoryItemsModel
import com.techwarsolutions.urmenuz.waiter.model.MyTablesModel
import kotlinx.android.synthetic.main.fragment_resturant_menu.*


class RestaurantMenuFragment : Fragment(), MenuInterface.OnMenuClickListener {

    var categoriesArrayList: ArrayList<CategoriesListModel>? = null
    var categoryHeaderArrayList: ArrayList<CategoryHeaderModel>? = null
    var categoryItemsArrayList: ArrayList<CategoryItemsModel>? = null
    var swipeListener: SwipeListener? = null

    var sharePreference: SharedPreference? = null
    var headerAdapter: CategoryHeaderAdapter? = null
    var edtSearchMenu: EditText? = null
    var btnViewTab: RelativeLayout? = null
    var itemsAdapter: CategoryItemsAdapter? = null
    var categoryHeadersList = arrayOf("Appetizer", "Salads", "Drinks")
    var text: String? = null
    var tableModel: MyTablesModel? = null


    private var item_name_list =
        arrayOf("Pizza", "Burger", "Garlic Bread", "Garlic Bread")
    private var item_decription_list =
        arrayOf(
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing"
        )


    var categoryIcons =
        arrayOf(R.drawable.subcat1, R.drawable.subcat2, R.drawable.subcat3, R.drawable.subcat1)

    var categoryName =
        arrayOf("Appetizers", "Salads", "Drinks", "Fruits")
    var previousPosition = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_resturant_menu, container, false)

        edtSearchMenu = view.findViewById(R.id.edtSearchMenu)
        btnViewTab = view.findViewById(R.id.btnOpenTabs)

        sharePreference = SharedPreference(activity)
        if (sharePreference?.addToTab.equals("true")) {
            btnViewTab?.visibility = View.VISIBLE
        }



        return view
    }

    override fun onResume() {
        super.onResume()

        sharePreference = SharedPreference(activity)
        if (sharePreference?.addToTab.equals("true")) {
            btnViewTab?.visibility = View.VISIBLE
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeListener = SwipeListener(swipeRestaurantMenu, requireActivity())
        swipeListener = SwipeListener(swipeRestaurantMenu2, requireActivity())

        sharePreference = SharedPreference(activity)
        if (sharePreference?.addToTab.equals("true")) {
            btnViewTab?.visibility = View.VISIBLE
        }
        if (sharePreference?.userImage != null && (sharePreference?.userImage)?.isNotEmpty()!!) {
            try {
                Glide.with(requireActivity())
                    .load(sharePreference?.userImage)
                    .placeholder(R.drawable.user)
                    .into(btnOpenWaiterSetting!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }

        val bundle = arguments
        if (bundle != null) {
            tableModel =
                Gson().fromJson(bundle.getString("tableInformation"), MyTablesModel::class.java)

            edtTableNumber?.setHint(tableModel?.tableNo)
            if (tableModel?.tableOccupiedStatus.equals("Occupied")) {
                // tblUnOccupied.visibility = View.GONE
                edtTableNumber?.isEnabled = false
                /*  txtCustomerName?.isEnabled = false
                  txtNoOrders.visibility = View.GONE
                  tblOccupied.visibility = View.VISIBLE*/
            } else {
                /* // tblUnOccupied.visibility = View.VISIBLE
                 txtNoOrders.visibility = View.VISIBLE
                 tblOccupied.visibility = View.GONE*/
            }

        }

        btnViewTab?.setOnClickListener {
            try {
                val bundle1 = Bundle()
                bundle1.putString("tableInformation", Gson().toJson(tableModel))
                val nextFrag = TabsFragment()
                nextFrag.arguments = bundle1
                setFragment(nextFrag)

                //setFragment(TabsFragment())

            } catch (e: Exception) {
                Log.e("OccupyDialog", e.toString())
            }
        }
        btnOpenWaiterSetting?.setOnClickListener {
            setFragment(WaiterSettingsFragment())
        }

        btnBackMenu?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }


        edtSearchMenu?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                try {
                    text = edtSearchMenu?.text.toString().toLowerCase()
                    itemsAdapter?.filter?.filter(text)
                } catch (e: Exception) {
                    Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        })

        btnOpenSearch?.setOnClickListener {
            //activity?.overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
            searchLayout?.visibility = View.VISIBLE
            categoryLayout?.visibility = View.GONE
        }
        closeSearch?.setOnClickListener {
            // activity?.overridePendingTransition(R.anim.animation_leave, R.anim.animation_enter);
            searchLayout?.visibility = View.GONE
            categoryLayout?.visibility = View.VISIBLE
        }

        categoryHeaderArrayList = java.util.ArrayList()
        for (MainCategory in categoryHeadersList) {
            categoryItemsArrayList = java.util.ArrayList()
            for (i in item_name_list.indices) {
                val model =
                    CategoryItemsModel(
                        item_name_list[i], item_decription_list[i]
                    )
                categoryItemsArrayList!!.add(model)
            }
            val mainTopModel =
                CategoryHeaderModel(MainCategory, categoryItemsArrayList)
            categoryHeaderArrayList!!.add(mainTopModel)
        }

        val adapter = CategoryHeaderAdapter(activity, categoryHeaderArrayList, this, tableModel)
        val mLayoutManager = LinearLayoutManager(activity)
        recyclerView_Main.layoutManager = mLayoutManager
        recyclerView_Main!!.adapter = adapter

        categoriesArrayList = java.util.ArrayList()
        for (i in categoryIcons.indices) {
            val model = CategoriesListModel(
                categoryIcons[i], categoryName[i]
            )
            categoriesArrayList!!.add(model)
        }
        categoriesArrayList!![0].isSelected = true
        val categoryAdapter = CategoriesListAdapter(activity, categoriesArrayList)
        rvCategories!!.adapter = categoryAdapter
        recyclerView_Main.addScrollListener { position: Int ->
            if (previousPosition != position) {
                categoriesArrayList?.get(previousPosition)?.isSelected = false
                categoriesArrayList?.get(position)?.isSelected = true
                previousPosition = position
            }
            rvCategories.adapter?.notifyDataSetChanged()
            rvCategories.smoothScrollToPosition(position)
        }
    }

    override fun OnMenuClick(view: View?, headerModel: CategoryHeaderModel?, position: Int) {
        //Toast.makeText(activity, headerModel?.categoryHeaderName, Toast.LENGTH_SHORT).show()
    }

    override fun onSelectMenuAction(changePosition: Int) {

    }

    private fun RecyclerView.addScrollListener(onScroll: (position: Int) -> Unit) {
        var lastPosition = 0
        addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (layoutManager is LinearLayoutManager) {
                    val currentVisibleItemPosition =
                        (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                    if (lastPosition != currentVisibleItemPosition && currentVisibleItemPosition != RecyclerView.NO_POSITION) {
                        onScroll.invoke(currentVisibleItemPosition)
                        lastPosition = currentVisibleItemPosition
                    }
                }
            }
        })
    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(
                0,
                0,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)
            ?.commit()
    }

}