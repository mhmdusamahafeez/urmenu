package com.techwarsolutions.urmenuz.waiter.interfaces;

import android.view.View;

import com.techwarsolutions.urmenuz.waiter.model.MyTablesModel;

public class TableInterfaces {

    public interface OnTableClickListener {
        void OnTableClick(View view, MyTablesModel tablesModel, int position);
        void onSelectTableAction(Boolean isSelected);

    }

}
