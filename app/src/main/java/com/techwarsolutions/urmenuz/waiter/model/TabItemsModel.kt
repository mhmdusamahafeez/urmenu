package com.techwarsolutions.urmenuz.waiter.model

class TabItemsModel {
    var tabItem: String? = null
    var tabItemQuantity: String? = null
    var tabItemPrice: String? = null
    var tabItemTotalPrice: String? = null

    constructor(
        tabItem: String?,
        tabItemQuantity: String?,
        tabItemPrice: String?,
    ) {
        this.tabItem = tabItem
        this.tabItemQuantity = tabItemQuantity
        this.tabItemPrice = tabItemPrice
    }
}