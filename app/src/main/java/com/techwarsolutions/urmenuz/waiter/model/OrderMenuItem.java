package com.techwarsolutions.urmenuz.waiter.model;


public class OrderMenuItem {
   String categoryName ;
   String itemName ;
   String itemDescription ;
   int categoryPosition ;

    public int getCategoryPosition() {
        return categoryPosition;
    }

    public void setCategoryPosition(int categoryPosition) {
        this.categoryPosition = categoryPosition;
    }

    public OrderMenuItem(String categoryName, String itemName  , String itemDescription  , int catPosition) {
        this.categoryName = categoryName;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.categoryPosition = catPosition;
    }
    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }
}
