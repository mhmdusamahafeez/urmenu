package com.techwarsolutions.urmenuz.waiter.model;

public class CategoryItemsModel {
    int RestaurantImage;
    String CategoryItemName ;
    String txtCategoryItemDescription ;

    public CategoryItemsModel(String categoryItemName, String txtCategoryItemDescription) {
        CategoryItemName = categoryItemName;
        this.txtCategoryItemDescription = txtCategoryItemDescription;
    }

    public int getRestaurantImage() {
        return RestaurantImage;
    }

    public void setRestaurantImage(int RestaurantImage) {
        RestaurantImage = RestaurantImage;
    }

    public String getTxtCategoryItemDescription() {
        return txtCategoryItemDescription;
    }

    public void setTxtCategoryItemDescription(String txtCategoryItemDescription) {
        this.txtCategoryItemDescription = txtCategoryItemDescription;
    }

    public String getCategoryItemName() {
        return CategoryItemName;
    }

    public void setCategoryItemName(String CategoryItemName) {
        this.CategoryItemName = CategoryItemName;
    }
}
