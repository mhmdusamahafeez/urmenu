package com.techwarsolutions.urmenuz.waiter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.waiter.interfaces.PreOrderInterface;
import com.techwarsolutions.urmenuz.waiter.model.PreviousOrderModel;

import java.util.ArrayList;

public class PreviousOrderAdapter extends RecyclerView.Adapter<PreviousOrderAdapter.PreviousOrderVH> {

    Activity activity;
    ArrayList<PreviousOrderModel> categoriesArrayList;
    LayoutInflater inflater;
    PreOrderInterface.OnPreOrderClickListener orderClickListener;

    public PreviousOrderAdapter(Activity activity, ArrayList<PreviousOrderModel> categoriesArrayList, PreOrderInterface.OnPreOrderClickListener orderClickListener) {
        this.activity = activity;
        this.categoriesArrayList = categoriesArrayList;
        this.inflater = activity.getLayoutInflater();
        this.orderClickListener = orderClickListener;
    }

    @NonNull
    @Override
    public PreviousOrderVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.previous_order_inner_items, null);
        return new PreviousOrderVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PreviousOrderVH holder, @SuppressLint("RecyclerView") int position) {
        PreviousOrderModel orderModel = categoriesArrayList.get(position);
        holder.txtPreOrderNo.setText("#"+orderModel.getTxtPreOrderNo());
        holder.txtPreOrderDateTime.setText(orderModel.getTxtPreOrderDateTime());
        holder.txtPaymentMethod.setText(orderModel.getTxtPaymentMethod());
        holder.txtPreOrderAmount.setText(orderModel.getTxtPreOrderAmount().toString()+"0 SR.");

        holder.preOrderSummary.setOnClickListener(
                view -> orderClickListener.OnPreOrderClick(view , orderModel , position)
        );

        if(orderModel.getTxtPaymentMethod().equals("Cash")){
            holder.cashIcon.setImageResource(R.drawable.money);
        }else {
            holder.cashIcon.setImageResource(R.drawable.ic_credit_method);
        }

    }

    @Override
    public int getItemCount() {
        return categoriesArrayList.size();
    }

    public static class PreviousOrderVH extends RecyclerView.ViewHolder {
        ImageView cashIcon;
        TextView txtPreOrderNo;
        TextView txtPreOrderDateTime;
        TextView txtPaymentMethod;
        TextView txtPreOrderAmount;
        LinearLayout preOrderSummary;

        public PreviousOrderVH(@NonNull View itemView) {
            super(itemView);
            cashIcon = itemView.findViewById(R.id.cashIcon);
            txtPreOrderNo = itemView.findViewById(R.id.txtPreOrderNo);
            txtPreOrderDateTime = itemView.findViewById(R.id.txtPreOrderDateTime);
            txtPaymentMethod = itemView.findViewById(R.id.txtPaymentMethod);
            txtPreOrderAmount = itemView.findViewById(R.id.txtPreOrderAmount);
            preOrderSummary = itemView.findViewById(R.id.preOrderSummary);
        }
    }
}
