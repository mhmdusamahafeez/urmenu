package com.techwarsolutions.urmenuz.waiter.interfaces;

import android.view.View;

import com.techwarsolutions.urmenuz.waiter.model.SuggestedItemModel;

public class SuggestedItemsInterfaces {

    public interface OnItemClickListener {

        void OnSuggestedItemClick(View view, SuggestedItemModel suggestedItemModel, int position);
        void onSelectedItemAction(Boolean isSelected);


    }
}
