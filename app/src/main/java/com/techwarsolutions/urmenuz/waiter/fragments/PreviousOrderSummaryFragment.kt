package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import com.techwarsolutions.urmenuz.waiter.model.PreviousOrderModel
import kotlinx.android.synthetic.main.fragment_item_description.*
import kotlinx.android.synthetic.main.fragment_previous_order_summary.*
import kotlinx.android.synthetic.main.fragment_previous_order_summary.btnBackPreOrder
import kotlinx.android.synthetic.main.fragment_previous_orders.*

class PreviousOrderSummaryFragment : Fragment() {

    var previousModel: PreviousOrderModel? = null
    var swipeListener: SwipeListener? = null
    private var sharedPreference: SharedPreference? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_previous_order_summary, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeListener = SwipeListener(swipePreOrderSummary, requireActivity())
        sharedPreference = SharedPreference(activity)

        if (sharedPreference?.userImage != null && (sharedPreference?.userImage)?.isNotEmpty()!!) {
            try {
                Glide.with(requireActivity())
                    .load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user)
                    .into(btnOpenWaiterSetting!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }
        btnBackPreOrder?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }

        val bundle = arguments
        if (bundle != null) {
            previousModel = Gson().fromJson(
                bundle.getString("preOrderInformation"),
                PreviousOrderModel::class.java
            )
            txtGetPaymentType?.text = previousModel?.txtPaymentMethod+" Payment"
            txtPreOrderNo.text = "Order # " + previousModel?.txtPreOrderNo
            txtOrderTotal.text = previousModel?.txtPreOrderAmount.toString() + "0 SR"

            val subTotal = previousModel?.txtPreOrderAmount
            val total = subTotal?.minus(10.00)
            txtSubTotal?.text = total.toString() + "0 SR"

        }
    }


}