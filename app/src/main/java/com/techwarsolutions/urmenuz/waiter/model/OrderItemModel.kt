package com.techwarsolutions.urmenuz.waiter.model

class OrderItemModel {

    var orderItemQuantity: String? = null
    var orderItemName: String? = null
    var orderItemPrice: String? = null

    constructor(orderItemQuantity: String?, orderItemName: String?, orderItemPrice: String?) {
        this.orderItemQuantity = orderItemQuantity
        this.orderItemName = orderItemName
        this.orderItemPrice = orderItemPrice
    }
}