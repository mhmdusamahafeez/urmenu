package com.techwarsolutions.urmenuz.waiter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.waiter.interfaces.MenuItemInterface;
import com.techwarsolutions.urmenuz.waiter.model.CategoryItemsModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryItemsAdapter extends RecyclerView.Adapter<CategoryItemsAdapter.CategoryItemsViewHolder>  implements Filterable  {

    Activity activity;
    ArrayList<CategoryItemsModel> categoryItemArrayList;
    LayoutInflater inflater;
    CustomSearchFilter filter;
    MenuItemInterface.OnMenuItemClickListener onMenuItemClickListener;

    public CategoryItemsAdapter(Activity activity, ArrayList<CategoryItemsModel> categoryItemArrayList, MenuItemInterface.OnMenuItemClickListener onMenuItemClickListener) {
        this.activity = activity;
        this.categoryItemArrayList = categoryItemArrayList;
        this.inflater = activity.getLayoutInflater();
        this.onMenuItemClickListener = onMenuItemClickListener;
    }


    @NonNull
    @Override
    public CategoryItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.category_inner_list_items, null);
        return new CategoryItemsViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull CategoryItemsViewHolder holder, @SuppressLint("RecyclerView") int position) {
        CategoryItemsModel model = categoryItemArrayList.get(position);
        holder.txtCategoryItemName.setText(model.getCategoryItemName());
        holder.txtCategoryItemDescription.setText(model.getTxtCategoryItemDescription());

        holder.itemClickLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMenuItemClickListener.OnMenuItemClick(view , model , position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryItemArrayList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomSearchFilter(categoryItemArrayList, this);
        }
        return filter;
    }


    public class CategoryItemsViewHolder extends RecyclerView.ViewHolder {
        ImageView restaurantImage;
        TextView txtCategoryItemName, txtCategoryItemDescription;
        RelativeLayout itemClickLayout;

        public CategoryItemsViewHolder(@NonNull View itemView) {
            super(itemView);

            itemClickLayout = itemView.findViewById(R.id.itemClickLayout);
            restaurantImage = itemView.findViewById(R.id.restaurantImage);
            txtCategoryItemDescription = itemView.findViewById(R.id.txtCategoryItemDescription);
            txtCategoryItemName = itemView.findViewById(R.id.txtCategoryItemName);
        }
    }

    public class CustomSearchFilter extends Filter {
        CategoryItemsAdapter adapter;
        List<CategoryItemsModel> filterList;

        public CustomSearchFilter(List<CategoryItemsModel> filterList, CategoryItemsAdapter adapter) {
            this.adapter = adapter;
            this.filterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                ArrayList<CategoryItemsModel> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getCategoryItemName().toLowerCase().contains(((String) constraint).toLowerCase())) {
                        filteredPlayers.add(filterList.get(i));
                    }
                }

                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = filterList.size();
                results.values = filterList;

            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            categoryItemArrayList = (ArrayList<CategoryItemsModel>) results.values;
            notifyDataSetChanged();
        }

    }
}
