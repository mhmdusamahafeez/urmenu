package com.techwarsolutions.urmenuz.waiter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.waiter.interfaces.UrTabInterfaces;
import com.techwarsolutions.urmenuz.waiter.model.TabItemsModel;

import java.util.ArrayList;

public class TabItemAdapter extends RecyclerView.Adapter<TabItemAdapter.TabViewHolder> {

    Activity activity;
    ArrayList<TabItemsModel> categoriesArrayList;
    LayoutInflater inflater;
    UrTabInterfaces.OnTabClickListener onTabClickListener;

    public TabItemAdapter(Activity activity, ArrayList<TabItemsModel> categoriesArrayList, UrTabInterfaces.OnTabClickListener onTabClickListener) {
        this.activity = activity;
        this.categoriesArrayList = categoriesArrayList;
        this.inflater = activity.getLayoutInflater();
        this.onTabClickListener = onTabClickListener;
    }


    @NonNull
    @Override
    public TabViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_items_tab, null);
        return new TabViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TabViewHolder holder, @SuppressLint("RecyclerView") int position) {
        TabItemsModel tabItemsModel = categoriesArrayList.get(position);
        holder.tabItemName.setText(tabItemsModel.getTabItem());
        holder.tabItemQuantity.setText(tabItemsModel.getTabItemQuantity());
        holder.txtTabItemPrice.setText(tabItemsModel.getTabItemPrice());
        holder.addTabQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String preQuantity = String.valueOf(holder.tabItemQuantity.getText());
                int pQ = Integer.parseInt(preQuantity);
                int nQ = pQ + 1;

                // String setQ = SrnQ;

                holder.tabItemQuantity.setText(String.valueOf(nQ));
                // onTabClickListener.addQuantityClick(view, tabItemsModel, position);
            }
        });

        holder.removeTabQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String preQuantity = String.valueOf(holder.tabItemQuantity.getText());
                int pQ = Integer.parseInt(preQuantity);
                int nQ = pQ - 1;
                if (nQ == 0) {

                } else {
                    holder.tabItemQuantity.setText(String.valueOf(nQ));
                }

                //onTabClickListener.removeQuantityClick(view, tabItemsModel, position);
            }
        });

        holder.btnRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTabClickListener.removeItemClick(view, tabItemsModel, position);
            }
        });

        holder.btnRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTabClickListener.removeItemClick(view, tabItemsModel, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categoriesArrayList.size();
    }

    public class TabViewHolder extends RecyclerView.ViewHolder {
        ImageView removeTabQuantity;
        ImageView addTabQuantity;
        ImageView btnRemoveItem;
        TextView tabItemName;
        TextView tabItemQuantity;
        TextView txtTabItemPrice;

        public TabViewHolder(@NonNull View itemView) {
            super(itemView);

            btnRemoveItem = itemView.findViewById(R.id.btnRemoveItem);
            removeTabQuantity = itemView.findViewById(R.id.removeTabQuantity);
            tabItemQuantity = itemView.findViewById(R.id.tabItemQuantity);
            addTabQuantity = itemView.findViewById(R.id.addTabQuantity);
            tabItemName = itemView.findViewById(R.id.tabItemName);
            txtTabItemPrice = itemView.findViewById(R.id.txtTabItemPrice);
        }
    }
}
