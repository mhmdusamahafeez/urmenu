package com.techwarsolutions.urmenuz.waiter.interfaces;

import android.view.View;

import com.techwarsolutions.urmenuz.waiter.model.CategoryHeaderModel;


public class MenuInterface {

    public interface OnMenuClickListener {
        void OnMenuClick(View view, CategoryHeaderModel headerModel, int position);
        void onSelectMenuAction(int changePosition);


    }
    
}
