package com.techwarsolutions.urmenuz.waiter.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.StringUtils
import com.techwarsolutions.urmenuz.waiter.interfaces.MenuInterface.OnMenuClickListener
import com.techwarsolutions.urmenuz.waiter.model.OrderMenuItem

class OrderMenuAdapter(
    var activity: Activity,
    var categoryItemArrayList: ArrayList<OrderMenuItem>,
    var menuClickListener: OnMenuClickListener
) : RecyclerView.Adapter<OrderMenuAdapter.CategoryItemsViewHolder>(), Filterable {
    var inflater: LayoutInflater = activity.layoutInflater
    var filter: CustomFilter? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryItemsViewHolder {
        val view = inflater.inflate(R.layout.category_inner_list_items, null)
        return CategoryItemsViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryItemsViewHolder, @SuppressLint("RecyclerView") position: Int) {

        val model = categoryItemArrayList[position]

        holder.txtCategoryItemName.text = model.itemName
        holder.txtCategoryItemDescription.text = model.itemDescription
        holder.txtCategoryName.text = model.categoryName


        val categoryName = model?.categoryName
        var previousName  = ""
        previousName = when {
            position > 0 -> {
                categoryItemArrayList[position-1]?.categoryName
            }
            position == 0 -> {
                ""
            }
            else -> {
                ""
            }
        }


        if(position == 0){
            holder.txtCategoryName.visibility = View.VISIBLE
            holder.txtCategoryName.text = "${categoryName}"
           // menuClickListener.onSelectMenuAction(model.categoryPosition)
        }else if(!StringUtils.isEmpty(previousName)){
            if(categoryName?.equals(previousName , ignoreCase = true) == false){
                holder.txtCategoryName.visibility = View.VISIBLE
                holder.txtCategoryName.text = "${categoryName}"
               // menuClickListener.onSelectMenuAction(model.categoryPosition)
            }else{
                holder.txtCategoryName.visibility = View.GONE
                holder.txtCategoryName.text = "${categoryName}"
            }
        }else{
            holder.txtCategoryName.visibility = View.GONE
            holder.txtCategoryName.text = "${categoryName}"
        }
//        holder.txtCategoryName.addOnAttachStateChangeListener(object :
//            View.OnAttachStateChangeListener {
//            override fun onViewAttachedToWindow(v: View) {
//                onPositionChangeListener.onItemChanged(position)
//            }
//
//            override fun onViewDetachedFromWindow(v: View) {}
//        })
//        holder.headerLayout.setOnClickListener { view ->
//            menuClickListener.OnMenuClick(
//                view,
//                null,
//                position
//            )
//        }
        holder.txtCategoryName.addOnAttachStateChangeListener(object :
            View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
               // menuClickListener.onSelectMenuAction(position)
            }

            override fun onViewDetachedFromWindow(v: View) {}
        })
    }

    override fun getItemCount(): Int {
        return categoryItemArrayList.size
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = CustomFilter(
                categoryItemArrayList, this
            )
        }
        return filter!!
    }

    inner class CategoryItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var restaurantImage: ImageView
        var txtCategoryItemName: TextView
        var txtCategoryItemDescription: TextView
        var txtCategoryName: TextView
        var headerLayout: RelativeLayout

        init {
            headerLayout = itemView.findViewById(R.id.headerLayout)
            txtCategoryName = itemView.findViewById(R.id.txtCategoryName)
            restaurantImage = itemView.findViewById(R.id.restaurantImage)
            txtCategoryItemDescription = itemView.findViewById(R.id.txtCategoryItemDescription)
            txtCategoryItemName = itemView.findViewById(R.id.txtCategoryItemName)

        }
    }

    inner class CustomFilter(var filterList: List<OrderMenuItem>, var adapter: OrderMenuAdapter) :
        Filter() {
        override fun performFiltering(constraint: CharSequence): FilterResults {
            var constraint: CharSequence? = constraint
            val results = FilterResults()
            if (constraint != null && constraint.length > 0) {
                constraint = constraint.toString().toUpperCase()
                val filteredPlayers = ArrayList<OrderMenuItem>()
                for (i in filterList.indices) {
                    if (filterList[i].itemName.toLowerCase().contains(constraint.toLowerCase())) {
                        filteredPlayers.add(filterList[i])
                    }
                }
                results.count = filteredPlayers.size
                results.values = filteredPlayers
            } else {
                results.count = filterList.size
                results.values = filterList
            }
            return results
        }

        override fun publishResults(constraint: CharSequence, results: FilterResults) {
            categoryItemArrayList = results.values as ArrayList<OrderMenuItem>
            notifyDataSetChanged()
        }
    }

    override fun getItemId(position: Int): Long {
        Log.e("TAG", "getItemId: "+position )
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        Log.e("TAG", "getItemViewType: "+position )
        return position
    }

}