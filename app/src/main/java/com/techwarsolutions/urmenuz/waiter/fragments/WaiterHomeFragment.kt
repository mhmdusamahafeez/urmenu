package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.ViewAdapter.ViewPagerAdapter
import com.techwarsolutions.urmenuz.utils.SharedPreference
import kotlinx.android.synthetic.main.fragment_home.txtDate
import kotlinx.android.synthetic.main.fragment_home.txtWeekDay
import kotlinx.android.synthetic.main.waiter_home_layout.*
import java.text.SimpleDateFormat
import java.util.*


class WaiterHomeFragment : Fragment() {

    private val TAG_FRAGMENT = "NewHomeFrag"
    private var sharedPreference: SharedPreference? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.waiter_home_layout, container, false)
    }

    private fun setCurrentDate() {
        val d = Date()
        val date: CharSequence = DateFormat.format("d MMMM, yyyy ", d.getTime())
        val calendar: Calendar = Calendar.getInstance()
        val day: Date = calendar.time
        txtWeekDay.text = SimpleDateFormat("EEEE", Locale.ENGLISH).format(day.getTime())
        txtDate.text = date
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//    }(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
        setCurrentDate()
        sharedPreference = SharedPreference(activity)

        btnOpenWaiterSetting?.setOnClickListener {
            setFragment(WaiterSettingsFragment())
        }
        if (sharedPreference?.userImage != null && (sharedPreference?.userImage)?.isNotEmpty()!!) {
            try {
                Glide.with(requireActivity())
                    .load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user)
                    .into(btnOpenWaiterSetting!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }
        try {
            val viewPagerAdapter = ViewPagerAdapter(childFragmentManager)
            viewPager.adapter = viewPagerAdapter

            viewPagerAdapter.addTabs(MyTablesFragment(), "My Tables")
            viewPagerAdapter.addTabs(TipsReceivedFragment(), "My Tips")
            viewPagerAdapter.notifyDataSetChanged()

            tabLayout.setupWithViewPager(viewPager)
            val tipView: View = layoutInflater.inflate(R.layout.custom_tab_tip, null)
            val tableView: View = layoutInflater.inflate(R.layout.custom_tab_table, null)

            viewPager.currentItem = 0
            val txtTipTab = tipView.findViewById<TextView>(R.id.txtTipTab)
            val txtMyTablesTab = tableView.findViewById<TextView>(R.id.txtMyTablesTab)
            txtTipTab.text = "  My Tips"
            txtMyTablesTab.text = "  My Tables"
            tabLayout.getTabAt(0)!!.customView = tableView
            tabLayout.getTabAt(1)!!.customView = tipView

        } catch (e: Exception) {
            Log.e("TabLayoutIssue", e.toString())
        }
    }


    override fun onStart() {
        super.onStart()
        try {
            val viewPagerAdapter = ViewPagerAdapter(childFragmentManager)
            viewPager.adapter = viewPagerAdapter

            viewPagerAdapter.addTabs(MyTablesFragment(), "My Tables")
            viewPagerAdapter.addTabs(TipsReceivedFragment(), "My Tips")
            viewPagerAdapter.notifyDataSetChanged()

            tabLayout.setupWithViewPager(viewPager)
            val tipView: View = layoutInflater.inflate(R.layout.custom_tab_tip, null)
            val tableView: View = layoutInflater.inflate(R.layout.custom_tab_table, null)

            viewPager.currentItem = 0
            val txtTipTab = tipView.findViewById<TextView>(R.id.txtTipTab)
            val txtMyTablesTab = tableView.findViewById<TextView>(R.id.txtMyTablesTab)
            txtTipTab.text = "My Tips"
            txtMyTablesTab.text = "  My Tables"
            tabLayout.getTabAt(0)!!.customView = tableView
            tabLayout.getTabAt(1)!!.customView = tipView

        } catch (e: Exception) {
            Log.e("TabLayoutIssue", e.toString())
        }

    }

    private fun setFragment(fragment: Fragment?) {
        try {
            activity?.supportFragmentManager?.beginTransaction()
                ?.setCustomAnimations(
                    0,
                    0,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right
                )
                ?.replace(R.id.waiterContainer, fragment!!)
                ?.addToBackStack(fragment.javaClass.canonicalName)
                ?.commit()

        } catch (e: Exception) {
            Log.e("TabLayoutIssue", e.toString())
        }


    }


}