package com.techwarsolutions.urmenuz.waiter.fragments

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import com.techwarsolutions.urmenuz.waiter.adapter.OrderItemAdapter
import com.techwarsolutions.urmenuz.waiter.model.MyTablesModel
import com.techwarsolutions.urmenuz.waiter.model.OrderItemModel
import kotlinx.android.synthetic.main.fragment_table_details.*


class TableDetailsFragment : Fragment() {

    var tableModel: MyTablesModel? = null
    private val statusList = arrayOf("Occupy", "Unoccupied")

    var orderItemsModel: OrderItemModel? = null
    private var orderItemArrayList: ArrayList<OrderItemModel>? = null
    var swipeListener: SwipeListener? = null
    private var sharedPreference: SharedPreference? = null


    private var tabItemList = arrayOf(
        "Vegetable Corn Soup",
        "Chicken Corn Soup",
    )
    private var tabItemQuantityList = arrayOf("3x", "4x")
    private var tabItemPriceList = arrayOf("23.00", "24.00")

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_table_details, container, false)

        container?.removeAllViews()

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeListener = SwipeListener(swipeTblDetails, requireActivity())
        sharedPreference = SharedPreference(activity)

        if (sharedPreference?.userImage != null && (sharedPreference?.userImage)?.isNotEmpty()!!) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(btnOpenWaiterSetting!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }


        btnOpenWaiterSetting?.setOnClickListener {
            setFragment(WaiterSettingsFragment())
        }
        btnBackTableDetails?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }


        val adapter: ArrayAdapter<String> =
            ArrayAdapter<String>(requireActivity(), R.layout.spinner_item, statusList)
        txtGetTblStatus?.adapter = adapter

        val bundle = arguments
        if (bundle != null) {
            tableModel =
                Gson().fromJson(bundle.getString("tableInformation"), MyTablesModel::class.java)

            txtSetOutTableNo?.text = "Tbl #" + tableModel?.tableNo
            txtGetTblGuests?.hint = tableModel?.tableGuestsNo
            txtTblOrderNo?.text = tableModel?.tableOrderNo
            tblOrderTime?.text = tableModel?.tableOrderTime

            if (tableModel?.tableType.equals("Hosted")) {
                customerType?.text = "Host Name:"
                txtCustomerName?.hint = tableModel?.tableHostName
                guestNameLayout?.visibility = View.VISIBLE
                tblGuestLayout?.visibility = View.VISIBLE
                rvCustomerItems?.visibility = View.GONE

                orderItemArrayList = java.util.ArrayList<OrderItemModel>()
                for (i in tabItemList.indices) {
                    val model = OrderItemModel(tabItemQuantityList.get(i),
                        tabItemList.get(i),
                        tabItemPriceList.get(i))

                    orderItemArrayList!!.add(model)
                }
                val adapter = OrderItemAdapter(activity, orderItemArrayList)
                rvHostItems.adapter = adapter

                orderItemArrayList = java.util.ArrayList<OrderItemModel>()
                for (i in tabItemList.indices) {
                    val model = OrderItemModel(tabItemQuantityList.get(i),
                        tabItemList.get(i),
                        tabItemPriceList.get(i))

                    orderItemArrayList!!.add(model)
                }
                val adapterGuestItems = OrderItemAdapter(activity, orderItemArrayList)
                rvGuest1Items.adapter = adapterGuestItems


            } else {
                customerType?.text = "Customer Name:"
                guestNameLayout?.visibility = View.GONE
                tblGuestLayout?.visibility = View.GONE
                rvCustomerItems?.visibility = View.VISIBLE

                orderItemArrayList = java.util.ArrayList<OrderItemModel>()
                for (i in tabItemList.indices) {
                    val model = OrderItemModel(tabItemQuantityList.get(i),
                        tabItemList.get(i),
                        tabItemPriceList.get(i))

                    orderItemArrayList!!.add(model)
                }
                val adapter = OrderItemAdapter(activity, orderItemArrayList)
                rvCustomerItems.adapter = adapter


            }


            if (tableModel?.orderPaymentStatus.equals("Cash")) {
                txtGetPaymentType?.text = "Cash Payment"
            } else if (tableModel?.orderPaymentStatus.equals("Unpaid")) {
                txtGetPaymentType?.text = "Cash Payment"
            }

            if (tableModel?.tableOccupiedStatus.equals("Occupied")) {
                txtGetTblStatus?.setSelection(0)
            } else {
                txtGetTblStatus?.setSelection(1)
            }

            if (tableModel?.orderPaymentStatus.equals("Paid")) {
                unpaidLayout.visibility = View.GONE
                paidLayout.visibility = View.VISIBLE

                if (tableModel?.orderPaymentStatus.equals("Cash")) {
                    txtGetPaymentType?.text = "Cash Payment"
                } else {
                    txtGetPaymentType?.text = "Credit Payment"
                }

            } else {
                unpaidLayout.visibility = View.VISIBLE
                paidLayout.visibility = View.GONE
            }

            if (tableModel?.tableOccupiedStatus.equals("Occupied")) {
                // tblUnOccupied.visibility = View.GONE
                txtGetTblGuests?.isEnabled = false
                txtCustomerName?.isEnabled = false
                txtNoOrders.visibility = View.GONE
                tblOccupied.visibility = View.VISIBLE
            } else {
                // tblUnOccupied.visibility = View.VISIBLE
                txtNoOrders.visibility = View.VISIBLE
                tblOccupied.visibility = View.GONE
            }

            tblUnOccupied?.setOnClickListener {
                try {
                    val bundle1 = Bundle()
                    bundle1.putString("tableInformation", Gson().toJson(tableModel))
                    val nextFrag = RestaurantMenuFragment()
                    nextFrag.arguments = bundle1
                    setFragment(nextFrag)

                } catch (e: Exception) {
                    Log.e("OccupyDialog", e.toString())
                }
            }

            //Close Order Click
            btnCloseOrder?.setOnClickListener {
                if (tableModel?.orderPaymentType?.equals("Cash") == true) {
                    try {
                        closeTableOrderDialog?.visibility = View.VISIBLE

                        closeTableOrderDialog.setBackgroundDrawable(BitmapDrawable(resources,
                            blur(requireActivity(), captureScreenShot(tblDetailSrc)!!)))

                        /*val inflater = requireActivity().layoutInflater
                        val view = inflater.inflate(R.layout.close_order_dialog, null)
                        val infoDialogBuilder = AlertDialog.Builder(activity)

                        val btnPaymentNotReceived: AppCompatButton =
                            view.findViewById(R.id.btnPaymentNotRecieved)
                        val btnPaymentReceived: AppCompatButton =
                            view.findViewById(R.id.btnPaymentReceived)

                        infoDialogBuilder.setView(view)
                        val infoDialog = infoDialogBuilder.create()
                        btnPaymentNotReceived?.setOnClickListener {
                            Toast.makeText(activity, "Please Collect Payments", Toast.LENGTH_SHORT)
                                .show()
                        }
                        btnPaymentReceived?.setOnClickListener {
                            if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                                activity?. supportFragmentManager?.popBackStack()
                                //setFragment(HomeFragment())
                            }
                            //setFragment(WaiterHomeFragment())
                            Toast.makeText(activity, "Order Completed", Toast.LENGTH_SHORT).show()
                            infoDialog?.dismiss()
                        }

                        infoDialog.setView(view)
                        infoDialog.show()
                        infoDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
*/
                    } catch (e: Exception) {
                        Log.e("CODDialog", e.toString())
                    }

                } else {
                    try {
                        setFragment(WaiterHomeFragment())
                    } catch (e: Exception) {
                        Log.e("PaidDialog", e.toString())
                    }
                }
            }

            //Close Table Order Dialog
            btnPaymentNotRecieved?.setOnClickListener {
                closeTableOrderDialog?.visibility = View.GONE
            }
            btnPaymentReceived?.setOnClickListener {
                closeTableOrderDialog?.visibility = View.GONE
            }
            closeTableOrderDialog?.setOnClickListener {
                closeTableOrderDialog?.visibility = View.GONE
            }


        } else {
            txtSetOutTableNo?.text = "Tbl #02"
            txtGetTblGuests?.hint = "02"
            txtTblOrderNo?.text = " #537"
            tblOrderTime?.text = " 6: 20 PM"
        }
    }


    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()

    }

    fun captureScreenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_4444)
        val canvas = Canvas(bitmap)
        val backgroundDrawable = view.background
        if (backgroundDrawable != null) {
            backgroundDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.parseColor("#80000000"))
        }
        view.draw(canvas)
        return bitmap
    }

    fun blur(context: Context?, image: Bitmap): Bitmap? {
        val BITMAP_SCALE = 0.4f
        val BLUR_RADIUS = 9.5f
        val width = Math.round(image.width * BITMAP_SCALE)
        val height = Math.round(image.height * BITMAP_SCALE)
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs = RenderScript.create(context)
        val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(BLUR_RADIUS)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }
}