package com.techwarsolutions.urmenuz.waiter

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.waiter.fragments.TableDetailsFragment
import com.techwarsolutions.urmenuz.waiter.fragments.WaiterHomeFragment

class WaiterDashboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_waiter_dashboard)

        loadFragment(WaiterHomeFragment())

    }


    private fun loadFragment(fragment: Fragment) {
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.waiterContainer, fragment)
        transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }

    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count > 1) {
            val visibleFragment = supportFragmentManager.fragments.findLast { fgm -> fgm.isVisible }
            if(visibleFragment is TableDetailsFragment){

                if(count > 2){
                    supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    loadFragment(WaiterHomeFragment())
                }else{
                    super.onBackPressed()
                    return
                }

              //
              //  supportFragmentManager.popBackStack("com.techwarsolutions.urmenuz.waiter.fragments.WaiterHomeFragment" , FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }else{
                super.onBackPressed() // Second Solution
                return
            }
            //supportFragmentManager.popBackStackImmediate() // First Solution

        } else {
            finishAffinity()
            //exitApp()
        }
    }

}