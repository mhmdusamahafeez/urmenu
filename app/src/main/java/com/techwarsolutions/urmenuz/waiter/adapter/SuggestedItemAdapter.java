package com.techwarsolutions.urmenuz.waiter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.waiter.interfaces.SuggestedItemsInterfaces;
import com.techwarsolutions.urmenuz.waiter.model.SuggestedItemModel;

import java.util.ArrayList;

public class SuggestedItemAdapter extends RecyclerView.Adapter<SuggestedItemAdapter.SuggestedItemViewHolder> {

    Activity activity;
    ArrayList<SuggestedItemModel> suggestedItemArrayList;
    LayoutInflater inflater;
    SuggestedItemsInterfaces.OnItemClickListener onItemClickListener;

    public SuggestedItemAdapter(Activity activity, ArrayList<SuggestedItemModel> suggestedItemArrayList, SuggestedItemsInterfaces.OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.suggestedItemArrayList = suggestedItemArrayList;
        this.inflater = activity.getLayoutInflater();
        this.onItemClickListener = onItemClickListener;
    }


    @NonNull
    @Override
    public SuggestedItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.suggested_items_list, null);
        return new SuggestedItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SuggestedItemViewHolder holder, @SuppressLint("RecyclerView") int position) {
        SuggestedItemModel suggestedItemModel = suggestedItemArrayList.get(position);
        holder.txtSuggestedItemPrice.setText(suggestedItemModel.getTxtSuggestedItemPrice());
        holder.suggestedItemImage.setBackgroundResource(suggestedItemModel.getSuggestedItemImage());

        holder.suggestedItemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(suggestedItemModel.getSelectedItem()){
                    holder.itemChecked.setVisibility(View.GONE);
                    suggestedItemModel.setSelectedItem(false);
                    notifyDataSetChanged();
                    if (getSelectedItems().size() == 0) {
                        onItemClickListener.onSelectedItemAction(false);
                    }
                }else {
                    suggestedItemModel.setSelectedItem(true);
                    notifyDataSetChanged();
                    holder.itemChecked.setVisibility(View.VISIBLE);
                    onItemClickListener.onSelectedItemAction(true);
                }

            }
        });

    }

    public ArrayList<SuggestedItemModel> getSelectedItems() {
        ArrayList<SuggestedItemModel> selectedItems = new ArrayList<>();
        for (SuggestedItemModel tablesModel : suggestedItemArrayList) {
            if (tablesModel.getSelectedItem()) {
                selectedItems.add(tablesModel);
            }
        }
        return selectedItems;
    }

    @Override
    public int getItemCount() {
        return suggestedItemArrayList.size();
    }

    public class SuggestedItemViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout suggestedItemImage;
        TextView txtSuggestedItemPrice;
        ImageView itemChecked;

        public SuggestedItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtSuggestedItemPrice = itemView.findViewById(R.id.txtSuggestedItemPrice);
            suggestedItemImage = itemView.findViewById(R.id.suggestedItemImage);
            itemChecked = itemView.findViewById(R.id.itemChecked);
        }
    }
}

