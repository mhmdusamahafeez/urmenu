package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.waiter.adapter.CategoriesListAdapter
import com.techwarsolutions.urmenuz.waiter.adapter.CategoryHeaderAdapter
import com.techwarsolutions.urmenuz.waiter.adapter.CategoryItemsAdapter
import com.techwarsolutions.urmenuz.waiter.adapter.OrderMenuAdapter
import com.techwarsolutions.urmenuz.waiter.interfaces.MenuInterface
import com.techwarsolutions.urmenuz.waiter.model.CategoriesListModel
import com.techwarsolutions.urmenuz.waiter.model.CategoryHeaderModel
import com.techwarsolutions.urmenuz.waiter.model.OrderMenuItem
import kotlinx.android.synthetic.main.fragment_resturant_menu.*

/*fun RecyclerView.addScrollListener(onScroll: (position: Int) -> Unit) {
    var lastPosition = 0
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (layoutManager is LinearLayoutManager) {
                val currentVisibleItemPosition =
                    (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

                if (lastPosition != currentVisibleItemPosition && currentVisibleItemPosition != RecyclerView.NO_POSITION) {
                    onScroll.invoke(currentVisibleItemPosition)
                    lastPosition = currentVisibleItemPosition
                }
            }
        }
    })
}*/

class OrderMenuFragment : Fragment(), MenuInterface.OnMenuClickListener {
    var categoriesArrayList: ArrayList<CategoriesListModel>? = null
    var categoryHeaderArrayList: ArrayList<CategoryHeaderModel>? = null
    var categoryItemsArrayList: ArrayList<OrderMenuItem>? = null

    var headerAdapter: CategoryHeaderAdapter? = null
    var itemsAdapter: CategoryItemsAdapter? = null
    var categoryHeadersList = arrayOf("Appetizer", "Salads", "Drinks")
    var text: String? = null


    private var item_name_list =
        arrayOf("Pizza", "Burger", "Garlic Bread", "Garlic Bread")
    private var item_decription_list =
        arrayOf(
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing",
            "Loreum ipsum dolor sit amet, conse ctetur adipiscing"
        )


    var categoryIcons =
        arrayOf(R.drawable.subcat1, R.drawable.subcat2, R.drawable.subcat3, R.drawable.subcat1)

    var categoryName =
        arrayOf("Appetizers", "Salads", "Drinks", "Fruits")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_resturant_menu, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btnBackMenu?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }

        edtSearchMenu.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                try {
                    text = edtSearchMenu.text.toString().toLowerCase()
                    itemsAdapter?.filter?.filter(text)
                } catch (e: Exception) {
                    Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show()
                }
            }
        })

        btnOpenSearch?.setOnClickListener {
            activity?.overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
            searchLayout?.visibility = View.VISIBLE
            categoryLayout?.visibility = View.GONE
        }
        closeSearch?.setOnClickListener {
            activity?.overridePendingTransition(R.anim.animation_leave, R.anim.animation_enter);
            searchLayout?.visibility = View.GONE
            categoryLayout?.visibility = View.VISIBLE
        }

        categoryHeaderArrayList = java.util.ArrayList()
        categoryItemsArrayList = ArrayList()
        for (categoryPosition in item_name_list.indices) {
            for (itemPosition in item_name_list.indices) {
                val model = OrderMenuItem(categoryName[categoryPosition] , item_name_list[itemPosition], item_decription_list[itemPosition] , categoryPosition)
                categoryItemsArrayList?.add(model)
            }

        }

        val adapter = categoryItemsArrayList?.let { activity?.let { it1 -> OrderMenuAdapter(it1, it, this) } }
        val mLayoutManager = LinearLayoutManager(activity)
        recyclerView_Main.layoutManager = mLayoutManager

/*        recyclerView_Main.addScrollListener { position: Int ->
            Log.e("TAG", "Current Position: $position ")
            Toast.makeText(activity, " Current Position $position", Toast.LENGTH_SHORT).show()
        }*/


        recyclerView_Main.adapter = adapter
        categoriesArrayList = java.util.ArrayList()
        for (i in categoryIcons.indices) {
            val model = CategoriesListModel(
                categoryIcons[i], categoryName[i]
            )
            categoriesArrayList!!.add(model)
        }
        val categoryAdapter = CategoriesListAdapter(activity, categoriesArrayList)
        rvCategories!!.adapter = categoryAdapter

    }

    override fun OnMenuClick(view: View?, headerModel: CategoryHeaderModel?, position: Int) {
        Toast.makeText(activity, headerModel?.categoryHeaderName, Toast.LENGTH_SHORT).show()
    }

    override fun onSelectMenuAction(changePosition: Int) {
        rvCategories.scrollToPosition(changePosition)
    }


}