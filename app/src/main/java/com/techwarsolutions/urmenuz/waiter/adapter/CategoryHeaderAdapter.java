package com.techwarsolutions.urmenuz.waiter.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.utils.SwipeListener;
import com.techwarsolutions.urmenuz.waiter.fragments.ItemDescriptionFragment;
import com.techwarsolutions.urmenuz.waiter.interfaces.MenuInterface;
import com.techwarsolutions.urmenuz.waiter.interfaces.MenuItemInterface;
import com.techwarsolutions.urmenuz.waiter.model.CategoryHeaderModel;
import com.techwarsolutions.urmenuz.waiter.model.CategoryItemsModel;
import com.techwarsolutions.urmenuz.waiter.model.MyTablesModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryHeaderAdapter extends RecyclerView.Adapter<CategoryHeaderAdapter.MainHeaderViewHolder> implements Filterable
        , MenuItemInterface.OnMenuItemClickListener {

    Activity activity;
    ArrayList<CategoryHeaderModel> categoryHeaderModelArrayList;
    LayoutInflater inflater;
    MenuInterface.OnMenuClickListener menuClickListener;
    CustomFilter filter;
    SwipeListener swipeListener;
    MyTablesModel model;


    public CategoryHeaderAdapter(Activity activity, ArrayList<CategoryHeaderModel> categoryHeaderModelArrayList,
                                 MenuInterface.OnMenuClickListener menuClickListener, MyTablesModel model) {
        this.activity = activity;
        this.categoryHeaderModelArrayList = categoryHeaderModelArrayList;
        this.inflater = activity.getLayoutInflater();
        this.menuClickListener = menuClickListener;
        this.model = model;
    }


    @NonNull
    @Override
    public MainHeaderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.category_header_list_items, null);
        return new MainHeaderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainHeaderViewHolder holder, @SuppressLint("RecyclerView") int position) {
        CategoryHeaderModel categoryHeaderModel = categoryHeaderModelArrayList.get(position);
        holder.txtCategoryHeaderName.setText(categoryHeaderModel.getCategoryHeaderName());
        CategoryItemsAdapter adapter = new CategoryItemsAdapter(activity, categoryHeaderModel.getCategoryItemsArrayList(), this);
        LinearLayoutManager manager = new LinearLayoutManager(activity);
        holder.recyclerView_SubCategory.setLayoutManager(manager);
        holder.recyclerView_SubCategory.setAdapter(adapter);
        swipeListener = new SwipeListener(holder.recyclerView_SubCategory, activity);

        holder.headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuClickListener.OnMenuClick(view, categoryHeaderModel, position);

            }
        });

    }

    @Override
    public int getItemCount() {
        return categoryHeaderModelArrayList.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilter(categoryHeaderModelArrayList, this);
        }

        return filter;
    }

    @Override
    public void OnMenuItemClick(View view, CategoryItemsModel itemsModel, int position) {

       /* Bundle bundle1 = new Bundle();
        bundle1.putString("tableInformation", new Gson().toJson(model));
        Fragment nextFrag = new ItemDescriptionFragment();
        nextFrag.setArguments(bundle1);
        setFragment(nextFrag);*/

        AppCompatActivity activity = (AppCompatActivity) view.getContext();
        Bundle bundle1 = new Bundle();
        bundle1.putString("tableInformation", new Gson().toJson(model));
        Fragment myFragment = new ItemDescriptionFragment();
        myFragment.setArguments(bundle1);
        activity.getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(0, 0, 0, R.anim.exit_to_right)
                .replace(R.id.waiterContainer, myFragment)
                .addToBackStack(myFragment.getClass().getName())
                .commit();

        //Toast.makeText(activity, model.getTableNo(), Toast.LENGTH_SHORT).show();
    }

    public class MainHeaderViewHolder extends RecyclerView.ViewHolder {
        TextView txtCategoryHeaderName;
        RecyclerView recyclerView_SubCategory;
        LinearLayout headerLayout;

        public MainHeaderViewHolder(@NonNull View itemView) {
            super(itemView);

            txtCategoryHeaderName = itemView.findViewById(R.id.txtCategoryHeaderName);
            headerLayout = itemView.findViewById(R.id.headerLayout);
            recyclerView_SubCategory = itemView.findViewById(R.id.rv_category_items);
        }
    }


  /*  public void setFragment(Fragment fragment) {
        activity.getFragmentManager().beginTransaction()
                .replace(R.id.waiterContainer , fragment)
                .addToBackStack("null")
                .commit();
    }*/


    public class CustomFilter extends Filter {
        CategoryHeaderAdapter adapter;
        List<CategoryHeaderModel> filterList;

        public CustomFilter(List<CategoryHeaderModel> filterList, CategoryHeaderAdapter adapter) {
            this.adapter = adapter;
            this.filterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                ArrayList<CategoryHeaderModel> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getCategoryHeaderName().toLowerCase().contains(((String) constraint).toLowerCase())) {
                        filteredPlayers.add(filterList.get(i));
                    }
                }

                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = filterList.size();
                results.values = filterList;

            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            categoryHeaderModelArrayList = (ArrayList<CategoryHeaderModel>) results.values;
            notifyDataSetChanged();
        }

    }


}
