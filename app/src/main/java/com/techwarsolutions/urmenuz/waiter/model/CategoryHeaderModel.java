package com.techwarsolutions.urmenuz.waiter.model;

import java.util.ArrayList;

public class CategoryHeaderModel {
    String categoryHeaderName;
    ArrayList<CategoryItemsModel> categoryItemsArrayList;

    public CategoryHeaderModel(String categoryHeaderName, ArrayList<CategoryItemsModel> categoryItemsArrayList) {
        this.categoryHeaderName = categoryHeaderName;
        this.categoryItemsArrayList = categoryItemsArrayList;
    }

    public String getCategoryHeaderName() {
        return categoryHeaderName;
    }

    public void setCategoryHeaderName(String categoryHeaderName) {
        this.categoryHeaderName = categoryHeaderName;
    }

    public ArrayList<CategoryItemsModel> getCategoryItemsArrayList() {
        return categoryItemsArrayList;
    }

    public void setCategoryItemsArrayList(ArrayList<CategoryItemsModel> categoryItemsArrayList) {
        this.categoryItemsArrayList = categoryItemsArrayList;
    }
}
