package com.techwarsolutions.urmenuz.waiter.fragments

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.waiter.adapter.MyTablesAdapter
import com.techwarsolutions.urmenuz.waiter.interfaces.TableInterfaces
import com.techwarsolutions.urmenuz.waiter.model.MyTablesModel
import kotlinx.android.synthetic.main.fragment_my_tables.*
import kotlinx.android.synthetic.main.fragment_waiter_settings.*


class MyTablesFragment : Fragment(), TableInterfaces.OnTableClickListener {
    var tablesModel: MyTablesModel? = null
    private var tableArrayList: ArrayList<MyTablesModel>? = null
    var onTablesAdapter: MyTablesAdapter? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_tables, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tableArrayList = ArrayList()
        tableArrayList!!.add(MyTablesModel(
            "1",
            "1",
            "3",
            "Usama",
            "#537",
            "6: 20 PM",
            "Occupied",
            "Confirmed Paid",
            "null",
            "Customer",
            "Paid",
            "Cash",
            ))

        tableArrayList!!.add(MyTablesModel("2",
            "2",
            "4",
            "Hafeez",
            "#538",
            "6: 30 PM",
            "Occupied",
            "Confirmed Unpaid",
            "null",
            "Hosted",
            "Unpaid",
            "Cash"))
        tableArrayList!!.add(MyTablesModel("3",
            "3",
            "3",
            "Abdullah",
            "#539",
            "6: 40 PM",
            "Occupied",
            "No Order",
            "null",
            "Customer",
            "Unpaid",
            "Cash"))

        tableArrayList!!.add(MyTablesModel("4",
            "4",
            "6",
            "Muhammad Raza",
            "#540",
            "6: 50 PM",
            "Unoccupied",
            "null",
            "Reserved",
            "Customer",
            "Unpaid",
            "Cash"))
        tableArrayList!!.add(MyTablesModel("5",
            "5",
            "4",
            "Zulkarnain",
            "#541",
            "6: 55 PM",
            "Unoccupied",
            "null",
            "Available",
            "Customer",
            "Unpaid",
            "Cash"))

        tableArrayList!!.add(MyTablesModel("6",
            "6",
            "4",
            "Jabran",
            "#542",
            "6: 59 PM",
            "Unoccupied",
            "null",
            "Available",
            "Customer",
            "Unpaid",
            "Cash"))



        if (tableArrayList != null) {
            onTablesAdapter = MyTablesAdapter(activity, tableArrayList, this)
            rvMyTables?.adapter = onTablesAdapter

        } else {
            //txtNoOnGoingOrder?.visibility = View.VISIBLE
        }


        btnSelectTbl?.setOnClickListener {
            tablesModel?.selectedTable = true
            onTablesAdapter?.setViewState("true", "VISIBLE", "Occupy")
            onTablesAdapter?.notifyDataSetChanged()
            occupyBtnLayout?.visibility = View.VISIBLE
            btnCancelTblSelect?.visibility = View.VISIBLE
            btnSelectTbl?.visibility = View.GONE

        }

        btnCancelTblSelect?.setOnClickListener {
            onTablesAdapter?.setViewState("false", "GONE", "Occupy")
            onTablesAdapter?.notifyDataSetChanged()
            occupyBtnLayout.visibility = View.GONE
            btnCancelTblSelect?.visibility = View.GONE

            btnCancelTblSelect?.visibility = View.GONE
            btnSelectTbl?.visibility = View.VISIBLE
        }
        btnOccupyTbl?.setOnClickListener {
            try {
                val selectedItem: ArrayList<MyTablesModel> = onTablesAdapter?.selectedOrders!!
                if (selectedItem.isEmpty()) {

                    mustSelectTblDialog.setBackgroundDrawable(BitmapDrawable(resources,
                        blur(requireActivity(), captureScreenShot(tblFullSrc)!!)))
                    mustSelectTblDialog.visibility = View.VISIBLE

                   /* val inflater = requireActivity().layoutInflater
                    val view = inflater.inflate(R.layout.must_select_dialog, null)
                    val infoDialogBuilder = AlertDialog.Builder(requireActivity())

                    val btnOkSelectItem: AppCompatButton = view.findViewById(R.id.btnOkSelectItem)
                    val dialogText: TextView = view.findViewById(R.id.dialogText)
                    infoDialogBuilder.setView(view)
                    val infoDialog = infoDialogBuilder.create()

                    dialogText.text = "Must select table to Unoccupy"
                    btnOkSelectItem.setOnClickListener {
                        infoDialog?.dismiss()
                    }

                    infoDialog.setView(view)
                    infoDialog.show()
                    infoDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)*/

                } else {
                    val tableNo = java.lang.StringBuilder()
                    for (i in selectedItem.indices) {
                        if (i == 0) {
                            tableNo.append(selectedItem[i].tableNo)
                        } else {
                            tableNo.append("\n").append(selectedItem[i].tableNo)
                        }
                    }
                    Toast.makeText(activity, "Tables Successfully Unoccupied", Toast.LENGTH_SHORT).show()
                    onTablesAdapter?.setViewState("false", "GONE", "unOccupy")
                    onTablesAdapter?.notifyDataSetChanged()
                    occupyBtnLayout.visibility = View.GONE
                    btnCancelTblSelect?.visibility = View.GONE
                    btnSelectTbl?.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                Log.e("NoItemSelected", e.toString())
            }
        }
        mustSelectTblDialog?.setOnClickListener {
            mustSelectTblDialog?.visibility = View.GONE
        }
        btnOkSelectItem.setOnClickListener {
            mustSelectTblDialog?.visibility = View.GONE
        }

    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()
    }

    override fun OnTableClick(view: View?, tablesModel: MyTablesModel?, position: Int) {
        val bundle = Bundle()
        bundle.putString("tableInformation", Gson().toJson(tablesModel))
        val nextFrag = TableDetailsFragment()
        nextFrag.arguments = bundle
        setFragment(nextFrag)
    }

    override fun onSelectTableAction(isSelected: Boolean?) {
        if (isSelected == true) {
            occupyBtnLayout?.visibility = View.VISIBLE
            onTablesAdapter?.notifyDataSetChanged()
        } else {
            occupyBtnLayout?.visibility = View.GONE
            onTablesAdapter?.notifyDataSetChanged()

        }
    }


    fun captureScreenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_4444)
        val canvas = Canvas(bitmap)
        val backgroundDrawable = view.background
        if (backgroundDrawable != null) {
            backgroundDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.parseColor("#80000000"))
        }
        view.draw(canvas)
        return bitmap
    }

    fun blur(context: Context?, image: Bitmap): Bitmap? {
        val BITMAP_SCALE = 0.4f
        val BLUR_RADIUS = 9.5f
        val width = Math.round(image.width * BITMAP_SCALE)
        val height = Math.round(image.height * BITMAP_SCALE)
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs = RenderScript.create(context)
        val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(BLUR_RADIUS)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }
}