package com.techwarsolutions.urmenuz.waiter.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import kotlinx.android.synthetic.main.fragment_boy_settings.*
import kotlinx.android.synthetic.main.fragment_waiter_settings.*
import kotlinx.android.synthetic.main.fragment_waiter_settings.setImage
import kotlinx.android.synthetic.main.fragment_waiter_settings.uploadImage
import java.io.IOException


class WaiterSettingsFragment : Fragment() {

    private val PICK_IMAGE_REQUEST = 71
    var mDownloadUrl: String? = null
    private var filePath: Uri? = null
    private var sharedPreference: SharedPreference? = null
    private var swipeListener: SwipeListener? = null


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_waiter_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeListener = SwipeListener(swipeWaiterSettings, requireActivity())
        sharedPreference = SharedPreference(activity)
        if (sharedPreference?.userImage != null && (sharedPreference?.userImage)?.isNotEmpty()!!) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(setWaiterImage!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }
        btnWaiterBackSettings?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
        btnFAQLayouts?.setOnClickListener {
            setFragment(WaiterFAQsFragment())
        }
        btnPreviousOrders?.setOnClickListener {
            setFragment(PreviousOrdersFragment())
        }

        uploadImage?.setOnClickListener {
            chooseImage()
        }
        setImage?.setOnClickListener {
            chooseImage()
        }

        // Edit Profile Dialog
        openEditProfile?.setOnClickListener {
            editProfileDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(relFullScreen)!!)))
            editProfileDialog.visibility = View.VISIBLE
        }
        btnUpdateWaiterInfo?.setOnClickListener {
            waiterAccUpdatedDialog.visibility = View.VISIBLE;
            editProfileDialog.visibility = View.GONE;

            waiterAccUpdatedDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(relFullScreen)!!)))

        }

        // Update Profile Dialog
        waiterAccUpdatedDialog?.setOnClickListener {
            waiterAccUpdatedDialog.visibility = View.GONE;
        }
        btnWaiterAccountUpdated?.setOnClickListener {
            waiterAccUpdatedDialog.visibility = View.GONE;
        }

        // Logout Dialog Action
        btnLogoutLayouts?.setOnClickListener {
            logoutCustomDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(relFullScreen)!!)))
            logoutCustomDialog.visibility = View.VISIBLE
        }
        btnLogout?.setOnClickListener {
            logoutCustomDialog.visibility = View.GONE;
        }

        //Change Language Dialog
        btnChangeLanguage?.setOnClickListener {
            languageDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(relFullScreen)!!)))
            languageDialog.visibility = View.VISIBLE
        }
        closeLangDialog?.setOnClickListener {
            languageDialog.setVisibility(View.GONE);
        }

        // Password Change Dialog
        btnPasswordLayout?.setOnClickListener {
            passwordDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(relFullScreen)!!)))
            passwordDialog.visibility = View.VISIBLE
        }
        btnClosePassDialog?.setOnClickListener {
            passwordDialog.setVisibility(View.GONE);
        }
        btnUpdatePassword?.setOnClickListener {
            val oldPass = edtOldPassword.text.toString().trim()
            val newPass = edtNewPassword.text.toString().trim()
            if (TextUtils.isEmpty(oldPass)) {
                Toast.makeText(activity, "Enter old password!", Toast.LENGTH_SHORT).show()
            } else if (TextUtils.isEmpty(newPass)) {
                Toast.makeText(activity, "Enter new password!", Toast.LENGTH_SHORT).show()
            } else {
                passwordDialog.visibility = View.GONE;
            }

        }


        //Dismiss Dialogs on outside dialog touch
        passwordDialog?.setOnClickListener {
            passwordDialog.setVisibility(View.GONE);
        }
        languageDialog?.setOnClickListener {
            languageDialog.setVisibility(View.GONE);
        }
        logoutCustomDialog?.setOnClickListener {
            logoutCustomDialog.setVisibility(View.GONE);
        }

    }

    private fun chooseImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        uploadImage?.visibility = View.GONE
        setImage?.visibility = View.VISIBLE
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            filePath = data.data
            try {
                val bitmap =
                    MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, filePath)
                //mDownloadUrl = filePath.toString()
                sharedPreference?.saveUserImage(filePath?.toString())
                setImage!!.setImageBitmap(bitmap)
                setWaiterImage!!.setImageBitmap(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()
    }

    fun captureScreenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_4444)
        val canvas = Canvas(bitmap)
        val backgroundDrawable = view.background
        if (backgroundDrawable != null) {
            backgroundDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.parseColor("#80000000"))
        }
        view.draw(canvas)
        return bitmap
    }

    fun blur(context: Context?, image: Bitmap): Bitmap? {
        val BITMAP_SCALE = 0.4f
        val BLUR_RADIUS = 9.5f
        val width = Math.round(image.width * BITMAP_SCALE)
        val height = Math.round(image.height * BITMAP_SCALE)
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs = RenderScript.create(context)
        val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(BLUR_RADIUS)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }
}