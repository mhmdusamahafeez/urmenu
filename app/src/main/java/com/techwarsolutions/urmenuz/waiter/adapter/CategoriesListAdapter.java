package com.techwarsolutions.urmenuz.waiter.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.waiter.model.CategoriesListModel;
import java.util.ArrayList;

public class CategoriesListAdapter extends RecyclerView.Adapter<CategoriesListAdapter.CategoryViewHolder> {

    Activity activity;
    ArrayList<CategoriesListModel> categoriesArrayList;
    LayoutInflater inflater;

    public CategoriesListAdapter(Activity activity, ArrayList<CategoriesListModel> categoriesArrayList) {
        this.activity = activity;
        this.categoriesArrayList = categoriesArrayList;
        this.inflater = activity.getLayoutInflater();
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_items_categories, null);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        CategoriesListModel mainTopModel = categoriesArrayList.get(position);
        holder.txtCategoryName.setText(mainTopModel.getTxtCategoryName());
        holder.categoryIcon.setImageResource(R.drawable.subcat1);
        if(mainTopModel.isSelected()){
            holder.background.setBackground(ContextCompat.getDrawable(activity , R.drawable.bg_three));
        }else {
            holder.background.setBackground(ContextCompat.getDrawable(activity , R.drawable.bg_four));
        }

    }

    @Override
    public int getItemCount() {
        return categoriesArrayList.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView categoryIcon;
        TextView txtCategoryName;
        RelativeLayout background;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            background = itemView.findViewById(R.id.background);
            categoryIcon = itemView.findViewById(R.id.categoryIcon);
            txtCategoryName = itemView.findViewById(R.id.txtCategoryName);
        }
    }
}
