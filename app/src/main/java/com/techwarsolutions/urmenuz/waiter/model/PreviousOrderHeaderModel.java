package com.techwarsolutions.urmenuz.waiter.model;

import java.util.ArrayList;

public class PreviousOrderHeaderModel {

    String preOrderHeaderName;
    ArrayList<PreviousOrderModel> orderModelArrayList;

    public PreviousOrderHeaderModel(String preOrderHeaderName, ArrayList<PreviousOrderModel> orderModelArrayList) {
        this.preOrderHeaderName = preOrderHeaderName;
        this.orderModelArrayList = orderModelArrayList;
    }

    public String getPreOrderHeaderName() {
        return preOrderHeaderName;
    }

    public void setPreOrderHeaderName(String preOrderHeaderName) {
        this.preOrderHeaderName = preOrderHeaderName;
    }

    public ArrayList<PreviousOrderModel> getOrderModelArrayList() {
        return orderModelArrayList;
    }

    public void setOrderModelArrayList(ArrayList<PreviousOrderModel> orderModelArrayList) {
        this.orderModelArrayList = orderModelArrayList;
    }
}
