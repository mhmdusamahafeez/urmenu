package com.techwarsolutions.urmenuz.waiter.interfaces;

import android.view.View;
import com.techwarsolutions.urmenuz.waiter.model.PreviousOrderModel;

public class PreOrderInterface {

    public interface OnPreOrderClickListener {
        void OnPreOrderClick(View view, PreviousOrderModel orderModel, int position);
    }

}
