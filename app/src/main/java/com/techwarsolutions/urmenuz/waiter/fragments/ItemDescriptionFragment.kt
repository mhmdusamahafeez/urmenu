package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import com.techwarsolutions.urmenuz.waiter.adapter.SuggestedItemAdapter
import com.techwarsolutions.urmenuz.waiter.interfaces.SuggestedItemsInterfaces
import com.techwarsolutions.urmenuz.waiter.model.MyTablesModel
import com.techwarsolutions.urmenuz.waiter.model.SuggestedItemModel
import kotlinx.android.synthetic.main.fragment_item_description.*


class ItemDescriptionFragment : Fragment(), SuggestedItemsInterfaces.OnItemClickListener {

    var cQ: Int? = 0
    var sharedPreference: SharedPreference? = null
    private var suggestedItemsList: ArrayList<SuggestedItemModel>? = null
    var suggestedItemAdapter: SuggestedItemAdapter? = null
    var swipeListener: SwipeListener? = null
    var tableModel: MyTablesModel? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_item_description, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedPreference = SharedPreference(activity)
        swipeListener = SwipeListener(swipeDescription, requireActivity())

        val bundle = arguments
        if (bundle != null) {
            tableModel =
                Gson().fromJson(bundle.getString("tableInformation"), MyTablesModel::class.java)


            btnAddToTab?.setOnClickListener {
                sharedPreference?.saveAddToTab("true")
             //   Toast.makeText(activity, tableModel?.getTableNo(), Toast.LENGTH_SHORT).show()
                try {
                    val bundle1 = Bundle()
                    bundle1.putString("tableInformation", Gson().toJson(tableModel))
                    val nextFrag = RestaurantMenuFragment()
                    nextFrag.arguments = bundle1
                    setFragment(nextFrag)

                } catch (e: Exception) {
                    Log.e("OccupyDialog", e.toString())
                }
                //setFragment(RestaurantMenuFragment())

                val selectedItem: ArrayList<SuggestedItemModel> =
                    suggestedItemAdapter?.selectedItems!!
                val orderNo = java.lang.StringBuilder()
                for (i in selectedItem.indices) {
                    if (i == 0) {
                        orderNo.append(selectedItem[i].txtSuggestedItemPrice)
                    } else {
                        orderNo.append("\n").append(selectedItem[i].txtSuggestedItemPrice)
                    }
                }
            }
        }
        backItemDescription?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }

        addQuantity?.setOnClickListener {
            cQ = (txtGetQuantity.text).toString().toInt()
            val nQ = cQ?.plus(1)

            txtGetQuantity.text = nQ.toString()

        }

        edtTxtSpecIns?.setOnClickListener {
            edtTxtSpecIns.isCursorVisible = true
        }

        removeQuantity?.setOnClickListener {
            cQ = (txtGetQuantity.text).toString().toInt()
            val nQ = cQ?.minus(1)
            if (cQ?.equals(0) == true) {

            } else {
                txtGetQuantity.text = nQ.toString()
            }


        }

        btnXLSize?.setOnClickListener {
            btnXLRdButton.isChecked = true
            btnLRdButton.isChecked = false
            btnMedRdButton.isChecked = false
            btnSmallRdButton.isChecked = false
        }

        btnLSize?.setOnClickListener {
            btnXLRdButton.isChecked = false
            btnLRdButton.isChecked = true
            btnMedRdButton.isChecked = false
            btnSmallRdButton.isChecked = false
        }

        btnMedSize?.setOnClickListener {
            btnXLRdButton.isChecked = false
            btnLRdButton.isChecked = false
            btnMedRdButton.isChecked = true
            btnSmallRdButton.isChecked = false
        }

        btnSmallSize?.setOnClickListener {
            btnXLRdButton.isChecked = false
            btnLRdButton.isChecked = false
            btnMedRdButton.isChecked = false
            btnSmallRdButton.isChecked = true
        }

        btnXTopping?.setOnClickListener {
            btnTopRdButton.isChecked = false
            btnXTopRdButton.isChecked = true
        }

        btnSomeTopping?.setOnClickListener {
            btnTopRdButton.isChecked = true
            btnXTopRdButton.isChecked = false
        }

        suggestedItemsList = ArrayList()
        suggestedItemsList?.add(
            SuggestedItemModel(
                R.drawable.pizza,
                "20 SR.",
            )
        )
        suggestedItemsList?.add(
            SuggestedItemModel(
                R.drawable.homeimage,
                "22 SR.",
            )
        )
        suggestedItemsList?.add(
            SuggestedItemModel(
                R.drawable.pizza,
                "20 SR.",
            )
        )
        suggestedItemsList?.add(
            SuggestedItemModel(
                R.drawable.homeimage,
                "22 SR.",
            )
        )

        if (suggestedItemsList != null) {
            suggestedItemAdapter =
                SuggestedItemAdapter(activity, suggestedItemsList, this)
            rvSuggestedItems?.adapter = suggestedItemAdapter

        } else {
            //txtNoOnGoingOrder?.visibility = View.VISIBLE
        }


    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(
                0,
                0,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)
            ?.commit()
    }

    override fun OnSuggestedItemClick(
        view: View?,
        suggestedItemModel: SuggestedItemModel?,
        position: Int
    ) {
        TODO("Not yet implemented")
    }

    override fun onSelectedItemAction(isSelected: Boolean?) {
        if (isSelected == true) {
            suggestedItemAdapter?.notifyDataSetChanged()
        } else {
            suggestedItemAdapter?.notifyDataSetChanged()

        }
    }


}