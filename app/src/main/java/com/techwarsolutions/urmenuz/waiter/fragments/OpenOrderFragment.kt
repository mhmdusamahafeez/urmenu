package com.techwarsolutions.urmenuz.waiter.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.techwarsolutions.urmenuz.R
import kotlinx.android.synthetic.main.fragment_open_order.*


class OpenOrderFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_open_order, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        openOrderDetails?.setOnClickListener {
            //setFragment(PreviousOrderSummaryFragment())
        }

    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(
                0,
                0,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            ?.replace(R.id.waiterContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)
            ?.commit()
    }
}