package com.techwarsolutions.urmenuz.deliveryBoy.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.deliveryBoy.interfaces.OnGoingOrderInterfaces;
import com.techwarsolutions.urmenuz.deliveryBoy.model.OnGoingOrdersModel;

import java.util.ArrayList;

public class OnGoingOrdersAdapter extends RecyclerView.Adapter<OnGoingOrdersAdapter.OnGoingOrdersViewHolder> {

    Activity activity;
    LayoutInflater inflater;
    ArrayList<OnGoingOrdersModel> onGoingOrdersList;
    OnGoingOrderInterfaces.OnGoingOrderClickListener onGoingOrderInterfaces;

    public OnGoingOrdersAdapter(Activity activity, ArrayList<OnGoingOrdersModel> onGoingOrdersList,
                                OnGoingOrderInterfaces.OnGoingOrderClickListener onGoingOrderInterfaces) {
        this.activity = activity;
        this.onGoingOrdersList = onGoingOrdersList;
        this.inflater = activity.getLayoutInflater();
        this.onGoingOrderInterfaces = onGoingOrderInterfaces;
    }


    @NonNull
    @Override
    public OnGoingOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_items_ongoing_orders, null);
        return new OnGoingOrdersAdapter.OnGoingOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OnGoingOrdersViewHolder holder, @SuppressLint("RecyclerView") int position) {
        OnGoingOrdersModel model = onGoingOrdersList.get(position);
        holder.txtOrderNo.setText("#" + model.getTxtOrderNo());
        holder.txtPaymentMode.setText(model.getTxtPaymentMode());
        holder.txtOrderPlacerName.setText(model.getTxtOrderPlacerName());
        holder.txtUserLocDistance.setText(model.getTxtUserLocDistance() + " KM");

        if (model.getTxtPaymentMode().equals("Cash")) {
            holder.cashIcon.setVisibility(View.VISIBLE);
        } else {
            holder.cashIcon.setVisibility(View.GONE);

        }




        holder.onGoingOrderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGoingOrderInterfaces.OnGoingOrderClick(view, model, position);
            }
        });
        holder.viewDistanceOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGoingOrderInterfaces.ViewOnMapClick(view, model, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return onGoingOrdersList.size();
    }

    class OnGoingOrdersViewHolder extends RecyclerView.ViewHolder {
        TextView txtOrderNo;
        TextView txtPaymentMode;
        TextView txtOrderPlacerName;
        TextView txtUserLocDistance;
        ImageView cashIcon;
        RelativeLayout onGoingOrderLayout;
        LinearLayout viewDistanceOnMap;

        public OnGoingOrdersViewHolder(@NonNull View itemView) {
            super(itemView);
            viewDistanceOnMap = itemView.findViewById(R.id.viewDistanceOnMap);
            onGoingOrderLayout = itemView.findViewById(R.id.onGoingOrderLayout);
            txtOrderNo = itemView.findViewById(R.id.txtOrderNo);
            txtPaymentMode = itemView.findViewById(R.id.txtPaymentMode);
            txtOrderPlacerName = itemView.findViewById(R.id.txtOrderPlacerName);
            txtUserLocDistance = itemView.findViewById(R.id.txtUserLocDistance);
            cashIcon = itemView.findViewById(R.id.cashIcon);
        }
    }

}
