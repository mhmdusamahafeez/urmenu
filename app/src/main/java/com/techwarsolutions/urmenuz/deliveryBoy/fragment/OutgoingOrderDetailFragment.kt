package com.techwarsolutions.urmenuz.deliveryBoy.fragment

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.deliveryBoy.adapter.SummaryItemsAdapter
import com.techwarsolutions.urmenuz.deliveryBoy.model.OnGoingOrdersModel
import com.techwarsolutions.urmenuz.deliveryBoy.model.SummaryItemsModel
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import kotlinx.android.synthetic.main.fragment_going_order_detail.*
import kotlinx.android.synthetic.main.fragment_home.*


class OutgoingOrderDetailFragment : Fragment() {

    private var onGoingOrdersModel: OnGoingOrdersModel? = null
    private var summaryItemsList: ArrayList<SummaryItemsModel>? = null
    var summaryItemsAdapter: SummaryItemsAdapter? = null
    var swipeListener: SwipeListener? = null
    private var sharedPreference: SharedPreference? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        container?.removeAllViews()

        return inflater.inflate(R.layout.fragment_going_order_detail, container, false)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipeListener = SwipeListener(swipeLeftOutgoing, requireActivity())
        swipeListener = SwipeListener(swipeLeftOutgoing2, requireActivity())

        sharedPreference = SharedPreference(activity)
        if (sharedPreference?.userImage != null) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(btnOpenUserSetting!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }

        btnBackOutGoOrder?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }


        val bundle = arguments
        if (bundle != null) {
            onGoingOrdersModel =
                Gson().fromJson(bundle.getString("onGoingOrders"), OnGoingOrdersModel::class.java)

            txtSetOutOrderNo?.text = "Order # " + onGoingOrdersModel?.txtOrderNo
            txtGetOrderPlacerName?.text = onGoingOrdersModel?.txtOrderPlacerName
            txtSubTotal?.text = onGoingOrdersModel?.txtOrderSubTotal + " SR"
            txtOrderTotal?.text = onGoingOrdersModel?.txtOrderTotal + " SR"

/*
            btnMakeCall?.setOnClickListener {
                try {
                    onGoingOrdersModel?.txtOrderPlacerNumber?.let { it1 -> callUser(it1) }

                } catch (e: Exception) {
                    Toast.makeText(activity, "Unable to call", Toast.LENGTH_SHORT).show()
                }
            }

            btnViewOnMap?.setOnClickListener {
                try {
                    val gmmIntentUri =
                        Uri.parse("google.navigation:q=${onGoingOrdersModel?.txtUserLocation}")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    startActivity(mapIntent)
                } catch (e: Exception) {
                    Toast.makeText(activity, "Unable to open map.", Toast.LENGTH_LONG).show()
                }
            }*/

            //Recall Order
            btnRecallOrder?.setOnClickListener {
                try {
                    /*   val inflater = requireActivity().layoutInflater
                       val view = inflater.inflate(R.layout.recall_order_dialog, null)
                       val infoDialogBuilder = AlertDialog.Builder(activity)

                       val btnYesReturned: AppCompatButton = view.findViewById(R.id.btnYesReturned)
                       val btnSubmitReason: AppCompatButton = view.findViewById(R.id.btnSubmitReason)
                       val chkNoResponse: CheckBox = view.findViewById(R.id.chkNoResponse)
                       val chkNotCollect: CheckBox = view.findViewById(R.id.chkNotCollect)
                       val chkRouteIssue: CheckBox = view.findViewById(R.id.chkRouteIssue)
                       val chkVehicleIssue: CheckBox = view.findViewById(R.id.chkVehicleIssue)
                       val chkOthers: CheckBox = view.findViewById(R.id.chkOthers)
                       val edtRecallReason: EditText = view.findViewById(R.id.edtRecallReason)
                       val recallReasonLayout: LinearLayout =
                           view.findViewById(R.id.recallReasonLayout)
                       val recallLayout: LinearLayout = view.findViewById(R.id.recallLayout)

                       infoDialogBuilder.setView(view)
                       val infoDialog = infoDialogBuilder.create()

                       btnYesReturned.setOnClickListener {
                           recallReasonLayout?.visibility = View.VISIBLE
                           recallLayout?.visibility = View.GONE
                       }
                       chkOthers.setOnClickListener {
                           if (chkOthers.isChecked) {

                               edtRecallReason.visibility = View.VISIBLE
                           } else {
                               edtRecallReason.visibility = View.GONE
                           }
                       }
                       btnSubmitReason.setOnClickListener {
                           var other = ""
                           var noResponse = ""
                           var notCollect = ""
                           var routeIssue = ""
                           var vehicleIssue = ""

                           if (chkOthers.isChecked) {
                               other = edtRecallReason.text.toString()
                           } else if (chkNoResponse.isChecked) {
                               noResponse = "No Response"
                           } else if (chkNotCollect.isChecked) {
                               notCollect = "Customer didn't collect delivery"
                           } else if (chkRouteIssue.isChecked) {
                               routeIssue = "Issue on route"
                           } else if (chkVehicleIssue.isChecked) {
                               vehicleIssue = "Vehicle issue"
                           } else {
                               if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                                   activity?.supportFragmentManager?.popBackStack()
                               }
                               Toast.makeText(activity, "Submitted", Toast.LENGTH_SHORT).show()
                           }

                           infoDialog?.dismiss()

                       }

                       infoDialog.setView(view)
                       infoDialog.show()
                       infoDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)*/
                    recallDialog.setBackgroundDrawable(BitmapDrawable(resources,
                        blur(requireActivity(), captureScreenShot(swipeLeftOutgoing)!!)))
                    recallDialog.visibility = View.VISIBLE

                } catch (e: Exception) {
                    Log.e("RecallDialog", e.toString())
                }
            }
            btnYesReturned?.setOnClickListener {
                recallReasonDialog.setBackgroundDrawable(BitmapDrawable(resources,
                    blur(requireActivity(), captureScreenShot(swipeLeftOutgoing)!!)))
                recallReasonDialog.visibility = View.VISIBLE
                recallDialog.visibility = View.GONE

            }
            recallDialog?.setOnClickListener {
                recallDialog.visibility = View.GONE
            }

            //Recall Reason
            chkOthers.setOnClickListener {
                if (chkOthers.isChecked) {
                    edtRecallReason.visibility = View.VISIBLE
                } else {
                    edtRecallReason.visibility = View.GONE
                }
            }
            btnSubmitReason.setOnClickListener {
                if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                    activity?.supportFragmentManager?.popBackStack()
                }
                Toast.makeText(activity, "Submitted", Toast.LENGTH_SHORT).show()
            }
            recallReasonDialog?.setOnClickListener {
                recallReasonDialog.visibility = View.GONE
            }

            //Order Delivered
            btnDeliverOrder?.setOnClickListener {
                if (onGoingOrdersModel?.txtPaymentMode?.equals("Cash") == true) {
                    try {

                        /*  val inflater = requireActivity().layoutInflater
                          val view = inflater.inflate(R.layout.payment_cod_dialog, null)
                          val infoDialogBuilder = AlertDialog.Builder(activity)

                          val btnPaymentNotRecieved: AppCompatButton =
                              view.findViewById(R.id.btnPaymentNotRecieved)
                          val btnPaymentReceived: AppCompatButton =
                              view.findViewById(R.id.btnPaymentReceived)

                          infoDialogBuilder.setView(view)
                          val infoDialog = infoDialogBuilder.create()
                          btnPaymentNotRecieved?.setOnClickListener {
                              Toast.makeText(activity, "Please Collect Payments", Toast.LENGTH_SHORT)
                                  .show()
                          }
                          btnPaymentReceived?.setOnClickListener {
                              if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                                  activity?.supportFragmentManager?.popBackStack()
                                  //setFragment(HomeFragment())
                              }

                              Toast.makeText(activity, "Order Completed", Toast.LENGTH_SHORT).show()
                              infoDialog?.dismiss()
                          }

                          infoDialog.setView(view)
                          infoDialog.show()
                          infoDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)*/

                        codDialog.setBackgroundDrawable(BitmapDrawable(resources,
                            blur(requireActivity(), captureScreenShot(swipeLeftOutgoing)!!)))
                        codDialog.visibility = View.VISIBLE

                    } catch (e: Exception) {
                        Log.e("CODDialog", e.toString())
                    }

                } else {
                    try {
                        paidOrderLayout.setBackgroundDrawable(BitmapDrawable(resources,
                            blur(requireActivity(), captureScreenShot(swipeLeftOutgoing)!!)))
                        paidOrderLayout.visibility = View.VISIBLE


                        /*  val inflater = requireActivity().layoutInflater
                          val view = inflater.inflate(R.layout.payment_paid_dialog, null)
                          val infoDialogBuilder = AlertDialog.Builder(activity)
                          val btnOrderComplete: AppCompatButton =
                              view.findViewById(R.id.btnOrderComplete)

                          infoDialogBuilder.setView(view)
                          val infoDialog = infoDialogBuilder.create()

                          btnOrderComplete.setOnClickListener {
                              if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                                  activity?.supportFragmentManager?.popBackStack()
                                  //setFragment(HomeFragment())
                              }
                              Toast.makeText(activity, "Order Completed", Toast.LENGTH_SHORT).show()
                              infoDialog?.dismiss()
                          }

                          infoDialog.setView(view)
                          infoDialog.show()
                          infoDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)*/

                    } catch (e: Exception) {
                        Log.e("PaidDialog", e.toString())
                    }
                }
            }

            //COD Order
            codDialog?.setOnClickListener {
                codDialog.visibility = View.GONE
            }
            btnPaymentNotReceived?.setOnClickListener {
                codDialog.visibility = View.GONE
                Toast.makeText(activity, "Please Collect Payments", Toast.LENGTH_SHORT).show()
            }
            btnPaymentReceived?.setOnClickListener {
                if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                    activity?.supportFragmentManager?.popBackStack()
                    //setFragment(HomeFragment())
                }
                Toast.makeText(activity, "Order Completed", Toast.LENGTH_SHORT).show()

            }

            //Paid Orders
            paidOrderLayout?.setOnClickListener {
                paidOrderLayout.visibility = View.GONE
            }
            btnOrderComplete.setOnClickListener {
                if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                    activity?.supportFragmentManager?.popBackStack()
                }
                Toast.makeText(activity, "Order Completed", Toast.LENGTH_SHORT).show()
            }


        }

        summaryItemsList = ArrayList()
        summaryItemsList?.add(SummaryItemsModel("1", "12", "Spring Roles", "2", "21.00"))
        summaryItemsList?.add(SummaryItemsModel("1", "12", "Vegetable Roles", "2", "21.00"))
        summaryItemsList?.add(SummaryItemsModel("1", "12", "Vegetable Corn Soup", "2", "21.00"))
        summaryItemsList?.add(SummaryItemsModel("1", "12", "Filite Popcorn", "2", "21.00"))
        summaryItemsList?.add(SummaryItemsModel("1", "12", "Spring Roles", "2", "21.00"))
        summaryItemsList?.add(SummaryItemsModel("1", "12", "Fajita Pizza", "2", "21.00"))

        if (summaryItemsList != null) {
            summaryItemsAdapter = SummaryItemsAdapter(activity, summaryItemsList)
            rvSummaryItems?.adapter = summaryItemsAdapter
        } else {
            txtNoOnGoingOrder?.visibility = View.VISIBLE
        }

        btnOpenUserSetting?.setOnClickListener {
            setFragment(BoySettingsFragment())
        }

    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.fragmentsContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()
    }

    private fun callUser(number: String) {
        try {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:$number")
            startActivity(callIntent)

        } catch (e: Exception) {
            Log.e("Error Calling", e.toString())
        }
    }

    fun captureScreenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_4444)
        val canvas = Canvas(bitmap)
        val backgroundDrawable = view.background
        if (backgroundDrawable != null) {
            backgroundDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.parseColor("#80000000"))
        }
        view.draw(canvas)
        return bitmap
    }

    fun blur(context: Context?, image: Bitmap): Bitmap? {
        val BITMAP_SCALE = 0.4f
        val BLUR_RADIUS = 9.5f
        val width = Math.round(image.width * BITMAP_SCALE)
        val height = Math.round(image.height * BITMAP_SCALE)
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs = RenderScript.create(context)
        val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(BLUR_RADIUS)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }


}