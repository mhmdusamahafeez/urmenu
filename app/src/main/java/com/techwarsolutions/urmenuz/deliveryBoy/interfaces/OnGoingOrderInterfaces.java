package com.techwarsolutions.urmenuz.deliveryBoy.interfaces;

import android.view.View;

import com.techwarsolutions.urmenuz.deliveryBoy.model.OnGoingOrdersModel;

public class OnGoingOrderInterfaces {

  public interface OnGoingOrderClickListener {
    void OnGoingOrderClick(View view, OnGoingOrdersModel onGoingOrdersModel, int position);
    void ViewOnMapClick(View view, OnGoingOrdersModel onGoingOrdersModel, int position);
  }



}
