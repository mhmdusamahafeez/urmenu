package com.techwarsolutions.urmenuz.deliveryBoy.interfaces;

import android.view.View;

import com.techwarsolutions.urmenuz.deliveryBoy.model.OnGoingOrdersModel;
import com.techwarsolutions.urmenuz.deliveryBoy.model.ReadyOrderModel;


public class ReadyOrdersInterface {

    public interface OnReadyOrderClickListener {
        void OnReadyOrderClick(View view, ReadyOrderModel readyOrderModel, int position);

        void ViewOnMapClick(View view, ReadyOrderModel readyOrderModel, int position);

        void onSelectOrderAction(Boolean isSelected);

    }
}
