package com.techwarsolutions.urmenuz.deliveryBoy.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.deliveryBoy.adapter.OnGoingOrdersAdapter
import com.techwarsolutions.urmenuz.deliveryBoy.interfaces.OnGoingOrderInterfaces
import com.techwarsolutions.urmenuz.deliveryBoy.model.OnGoingOrdersModel
import com.techwarsolutions.urmenuz.utils.SharedPreference
import kotlinx.android.synthetic.main.fragment_home.*
import java.text.SimpleDateFormat
import java.util.*


class HomeFragment : Fragment(), OnGoingOrderInterfaces.OnGoingOrderClickListener {

    var onGoingOrdersModel: OnGoingOrdersModel? = null
    private var onGoingArrayList: ArrayList<OnGoingOrdersModel>? = null
    var onGoingOrdersAdapter: OnGoingOrdersAdapter? = null
    private var sharedPreference: SharedPreference? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        container?.removeAllViews()

        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setCurrentDate()
        sharedPreference = SharedPreference(activity)
        if (sharedPreference?.userImage != null && (sharedPreference?.userImage)?.isNotEmpty()!!) {
            // settingIcon?.visibility = View.INVISIBLE
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(btnOpenUserSettings!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }

        settingsAppBar.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            var isShow = false
            var scrollRange = -1
            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true
                    //lockScrollView?.setScrollingEnabled(false)
                    //toShowLayout.visibility = View.VISIBLE
                    ic_down_arrow.visibility = View.VISIBLE
                    //hideText.visibility = View.INVISIBLE
                } else if (isShow) {
                    isShow = false
                    //lockScrollView?.setScrollingEnabled(true)
                    // toShowLayout.visibility = View.INVISIBLE
                    ic_down_arrow.visibility = View.INVISIBLE
                    //hideText.visibility = View.VISIBLE

                }
            }
        })

        btnReadyOrders?.setOnClickListener {
            setFragment(ReadyOrdersFragment())
        }
        btnOpenUserSettings?.setOnClickListener {
            setFragment(BoySettingsFragment())
        }
        btnNewOrders?.setOnClickListener {
            setFragment(NewOrdersFragment())
        }
        btnCompletedOrders?.setOnClickListener {
            setFragment(CompletedOrderFragment())
        }

        onGoingArrayList = ArrayList()
        onGoingArrayList?.add(OnGoingOrdersModel("1",
            "537",
            "20.00",
            "10.00",
            "21.00",
            "Usama Hafeez",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("2",
            "538",
            "220.00",
            "10.00",
            "230.00",
            "Muhammad Ali",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("3",
            "539",
            "250.00",
            "10.00",
            "260.00",
            "Muhammad Abdullah Abdurehman",
            "03478302658",
            "Cash",
            "10",
            "170/B Muhafiz Town, Sargodha"

        ))
        onGoingArrayList?.add(OnGoingOrdersModel("1",
            "540",
            "20.00",
            "10.00",
            "21.00",
            "Ali Raza",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("2",
            "541",
            "220.00",
            "10.00",
            "230.00",
            "Jabran Yousaf",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("1",
            "537",
            "20.00",
            "10.00",
            "21.00",
            "Usama Hafeez",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("2",
            "538",
            "220.00",
            "10.00",
            "230.00",
            "Muhammad Ali",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("3",
            "539",
            "250.00",
            "10.00",
            "260.00",
            "Muhammad Abdullah Abdurehman",
            "03478302658",
            "Cash",
            "10",
            "170/B Muhafiz Town, Sargodha"

        ))
        onGoingArrayList?.add(OnGoingOrdersModel("1",
            "540",
            "20.00",
            "10.00",
            "21.00",
            "Ali Raza",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("2",
            "541",
            "220.00",
            "10.00",
            "230.00",
            "Jabran Yousaf",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("1",
            "537",
            "20.00",
            "10.00",
            "21.00",
            "Usama Hafeez",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("2",
            "538",
            "220.00",
            "10.00",
            "230.00",
            "Muhammad Ali",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("3",
            "539",
            "250.00",
            "10.00",
            "260.00",
            "Muhammad Abdullah Abdurehman",
            "03478302658",
            "Cash",
            "10",
            "170/B Muhafiz Town, Sargodha"

        ))
        onGoingArrayList?.add(OnGoingOrdersModel("1",
            "540",
            "20.00",
            "10.00",
            "21.00",
            "Ali Raza",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(OnGoingOrdersModel("2",
            "541",
            "220.00",
            "10.00",
            "230.00",
            "Jabran Yousaf",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))

        if (onGoingArrayList != null) {
            onGoingOrdersAdapter = OnGoingOrdersAdapter(activity, onGoingArrayList, this)
            rvOnGoingOrders?.adapter = onGoingOrdersAdapter
            rvOnGoingOrders.isNestedScrollingEnabled = false;

        } else {
            txtNoOnGoingOrder?.visibility = View.VISIBLE
        }


    }

    override fun OnGoingOrderClick(view: View?,
                                   onGoingOrdersModel: OnGoingOrdersModel?,
                                   position: Int) {
        val bundle = Bundle()
        bundle.putString("onGoingOrders", Gson().toJson(onGoingOrdersModel))
        val nextFrag = OutgoingOrderDetailFragment()
        nextFrag.arguments = bundle
        setFragment(nextFrag)
    }

    private fun setCurrentDate() {
        val d = Date()
        val date: CharSequence = DateFormat.format("d MMMM, yyyy ", d.getTime())
        val calendar: Calendar = Calendar.getInstance()
        val day: Date = calendar.getTime()
        txtWeekDay.setText(SimpleDateFormat("EEEE", Locale.ENGLISH).format(day.getTime()))
        txtDate.text = date
    }


    override fun ViewOnMapClick(view: View?,
                                onGoingOrdersModel: OnGoingOrdersModel?,
                                position: Int) {
        try {
            val gmmIntentUri =
                Uri.parse("google.navigation:q=${onGoingOrdersModel?.txtUserLocation}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        } catch (e: Exception) {
            Toast.makeText(activity, "Unable to open map.", Toast.LENGTH_LONG).show()
        }
    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.fragmentsContainer, fragment!!)

            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()
    }
}
