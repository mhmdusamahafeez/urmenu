package com.techwarsolutions.urmenuz.deliveryBoy.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.deliveryBoy.model.SummaryItemsModel;

import java.util.ArrayList;

public class SummaryItemsAdapter extends RecyclerView.Adapter<SummaryItemsAdapter.OnGoingOrdersViewHolder> {

    Activity activity;
    LayoutInflater inflater;
    ArrayList<SummaryItemsModel> readyOrdersList;

    public SummaryItemsAdapter(Activity activity, ArrayList<SummaryItemsModel> readyOrdersList) {
        this.activity = activity;
        this.readyOrdersList = readyOrdersList;
        this.inflater = activity.getLayoutInflater();
    }


    @NonNull
    @Override
    public SummaryItemsAdapter.OnGoingOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_order_summary, null);
        return new OnGoingOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SummaryItemsAdapter.OnGoingOrdersViewHolder holder, @SuppressLint("RecyclerView") int position) {
        SummaryItemsModel model = readyOrdersList.get(position);
        holder.txtItemName.setText(model.getItemName());
        holder.txtItemQuantity.setText(model.getItemQuantity() + "X");
        holder.txtItemPrice.setText(model.getEachItemPrice() + " SR");

    }

    @Override
    public int getItemCount() {
        return readyOrdersList.size();
    }

    static class OnGoingOrdersViewHolder extends RecyclerView.ViewHolder {
        TextView txtItemQuantity;
        TextView txtItemName;
        TextView txtItemPrice;

        public OnGoingOrdersViewHolder(@NonNull View itemView) {
            super(itemView);
            txtItemQuantity = itemView.findViewById(R.id.txtItemQuantity);
            txtItemName = itemView.findViewById(R.id.txtItemName);
            txtItemPrice = itemView.findViewById(R.id.txtItemPrice);
        }
    }
}
