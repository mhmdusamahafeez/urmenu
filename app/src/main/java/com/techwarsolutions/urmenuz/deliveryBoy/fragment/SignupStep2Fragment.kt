package com.techwarsolutions.urmenuz.deliveryBoy.fragment

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.snackbar.Snackbar
import com.hbb20.CountryCodePicker
import com.techwarsolutions.urmenuz.LoginActivity
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.deliveryBoy.BoyDashboardActivity
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.waiter.WaiterDashboardActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_signup_step2.*
import kotlinx.android.synthetic.main.get_user_info_dialog.*

class SignupStep2Fragment() : Fragment() {

    var receivedEmail:String?=null



    private var sharedPreference: SharedPreference? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_signup_step2, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedPreference = SharedPreference(activity)

        hideCreatePassword?.setOnClickListener {
            showCreatePassword?.visibility = View.VISIBLE
            hideCreatePassword?.visibility = View.GONE

            edtCreatePassword.transformationMethod = PasswordTransformationMethod.getInstance()
        }
        showCreatePassword?.setOnClickListener {
            showCreatePassword?.visibility = View.GONE
            hideCreatePassword?.visibility = View.VISIBLE
            edtCreatePassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
        }

        hideConfirmPassword?.setOnClickListener {
            showConfirmPassword?.visibility = View.VISIBLE
            hideConfirmPassword?.visibility = View.GONE
            edtConfirmCPassword.transformationMethod = PasswordTransformationMethod.getInstance()
        }
        showConfirmPassword?.setOnClickListener {
            showConfirmPassword?.visibility = View.GONE
            hideConfirmPassword?.visibility = View.VISIBLE
            edtConfirmCPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
        }

        alreadyAccount?.setOnClickListener {
            val intent = Intent(activity, LoginActivity::class.java)
            startActivity(intent)
        }

        btnSignUp?.setOnClickListener {
            val createPassword = edtCreatePassword.text.toString().trim()
            val confirmPassword = edtConfirmCPassword.text.toString().trim()
            if(TextUtils.isEmpty(createPassword)){
                Toast.makeText(activity, "Enter your password", Toast.LENGTH_SHORT).show()


            }else if(TextUtils.isEmpty(confirmPassword)){
                Toast.makeText(activity, "Enter confirm password", Toast.LENGTH_SHORT).show()


            }else if(createPassword != confirmPassword){
                Toast.makeText(activity, "Password does not match", Toast.LENGTH_SHORT).show()

            }else{
                sharedPreference!!.saveUserPassword(confirmPassword)
                try {
                    val inflater = requireActivity().layoutInflater
                    val view = inflater.inflate(R.layout.get_user_info_dialog, null)
                    val infoDialogBuilder = AlertDialog.Builder(activity)
                    infoDialogBuilder.setView(view)
                    val infoDialog = infoDialogBuilder.create()

                    val pbLoading: ProgressBar = view.findViewById(R.id.pbLoading)
                    val getUserName: EditText = view.findViewById(R.id.getUserName)
                    val getUserNo: EditText = view.findViewById(R.id.getUserNo)
                    val btnAddUserInfo: AppCompatButton = view.findViewById(R.id.btnAddUserInfo)
                    val codePicker: CountryCodePicker = view.findViewById(R.id.ccp)

                    btnAddUserInfo.setOnClickListener {
                        pbLoading.visibility = View.VISIBLE
                        if (TextUtils.isEmpty(getUserName.text.toString())) {
                            //showErrorSnackBar("")
                            getUserName.error = "Please enter your name"
                            pbLoading.visibility = View.GONE
                            Toast.makeText(activity, "Please enter your name", Toast.LENGTH_SHORT).show()
                        } else if (TextUtils.isEmpty(getUserNo.text.toString())) {
                            //showErrorSnackBar("Please enter your phone number")
                            getUserNo.error = "Please enter your phone number"
                            pbLoading.visibility = View.GONE
                            Toast.makeText(activity, "Please enter your phone number", Toast.LENGTH_SHORT).show()

                        } else {
                           // pbLoading.visibility = View.GONE
                            sharedPreference!!.saveUserName(getUserName.text.toString())
                            sharedPreference!!.saveUserFullPhNo("+966"+getUserNo.text.toString() )
                            sharedPreference!!.saveUserPhNo( getUserNo.text.toString() )
                            sharedPreference!!.savePassCreated("true")

                            Toast.makeText(activity, "Registered successfully", Toast.LENGTH_SHORT).show()

                            val intent = Intent(activity, WaiterDashboardActivity::class.java)
                            //val intent = Intent(activity, BoyDashboardActivity::class.java)
                            startActivity(intent)

                        }


                    }

                    infoDialog.setView(view)
                    infoDialog.show()
                    infoDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
                    infoDialog.setCancelable(false)

                } catch (e: Exception) {
                    Log.e("ProfileDialog", e.toString())
                }
            }



        }


    }

    fun showErrorSnackBar(msg: String) {
        try {
            val snackbar =
                Snackbar.make(requireActivity().findViewById(android.R.id.content), msg, 2500)
            snackbar.setActionTextColor(resources.getColor(R.color.white))
            snackbar.view.setBackgroundColor(resources.getColor(R.color.light_brown))
            snackbar.show()
        } catch (e: Exception) {
            Log.e("SnackBarError", e.toString())
        }

    }


}