package com.techwarsolutions.urmenuz.deliveryBoy

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import cn.pedant.SweetAlert.SweetAlertDialog
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.deliveryBoy.fragment.HomeFragment

class BoyDashboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_boy_dashboard)

        loadFragment(HomeFragment())

    }

    private fun loadFragment(fragment: Fragment) {
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentsContainer, fragment)
        transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }
    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount!! > 1) {
            //supportFragmentManager.popBackStackImmediate() // First Solution
            super.onBackPressed() // Second Solution
            return
        } else {
            finishAffinity()
            //exitApp()
        }
    }
    private fun exitApp() {

        SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
            .setTitleText("Exit urMenuz")
            .setCustomImage(R.drawable.ic_splash_icon)
            .setContentText("Do you want to exit app")
            .setConfirmText("Yes")
            .setCancelText("No")
            .setCancelClickListener {
                it.dismissWithAnimation()
            }
            .setConfirmClickListener { sDialog -> // Showing simple toast message to user
                finishAffinity()
            }.show()
    }
}