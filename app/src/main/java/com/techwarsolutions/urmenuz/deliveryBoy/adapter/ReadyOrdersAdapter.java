package com.techwarsolutions.urmenuz.deliveryBoy.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.techwarsolutions.urmenuz.R;
import com.techwarsolutions.urmenuz.deliveryBoy.interfaces.ReadyOrdersInterface;
import com.techwarsolutions.urmenuz.deliveryBoy.model.ReadyOrderModel;

import java.util.ArrayList;

public class ReadyOrdersAdapter extends RecyclerView.Adapter<ReadyOrdersAdapter.OnGoingOrdersViewHolder> {

    Activity activity;
    LayoutInflater inflater;
    ArrayList<ReadyOrderModel> readyOrdersList;
    ReadyOrdersInterface.OnReadyOrderClickListener readyOrderClickListener;
    String state = "false";
    String visibility = "GONE";

    public ReadyOrdersAdapter(Activity activity, ArrayList<ReadyOrderModel> readyOrdersList,
                              ReadyOrdersInterface.OnReadyOrderClickListener readyOrderClickListener) {
        this.activity = activity;
        this.readyOrdersList = readyOrdersList;
        this.inflater = activity.getLayoutInflater();
        this.readyOrderClickListener = readyOrderClickListener;
    }


    @NonNull
    @Override
    public ReadyOrdersAdapter.OnGoingOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.ready_orders_layout, null);
        return new OnGoingOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReadyOrdersAdapter.OnGoingOrdersViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ReadyOrderModel model = readyOrdersList.get(position);
        holder.txtOrderNo.setText("#" + model.getTxtOrderNo());
        holder.txtPaymentMode.setText(model.getTxtPaymentMode());
        holder.txtOrderPlacerName.setText(model.getTxtOrderPlacerName());
        holder.txtUserLocDistance.setText(model.getTxtUserLocDistance() + " KM");
        if(visibility.equals("VISIBLE")){
            holder.itemUnChecked.setVisibility(View.VISIBLE);

        }else {
            model.setSelectedOrder(false);
            holder.itemUnChecked.setVisibility(View.INVISIBLE);
            holder.itemChecked.setVisibility(View.INVISIBLE);
        }
        if (model.getSelectedOrder()) {
            holder.itemChecked.setVisibility(View.VISIBLE);
        } else {
            holder.itemChecked.setVisibility(View.GONE);
        }



        if (model.getTxtPaymentMode().equals("Cash")) {
            holder.cashIcon.setVisibility(View.VISIBLE);
        } else {
            holder.cashIcon.setVisibility(View.GONE);
        }



        holder.onGoingOrderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyDataSetChanged();
                if ( model.getSelectedOrder()) {
                    holder.itemChecked.setVisibility(View.GONE);
                    model.setSelectedOrder(false);
                    if (getSelectedOrders().size() == 0) {
                        readyOrderClickListener.onSelectOrderAction(false);
                    }
                } else {
                    if(state.equals("true")){
                        model.setSelectedOrder(true);
                        holder.itemChecked.setVisibility(View.VISIBLE);
                        readyOrderClickListener.onSelectOrderAction(true);
                    }else {
                        readyOrderClickListener.OnReadyOrderClick(view, model, position);
                    }
                }
            }
        });
        holder.viewDistanceOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readyOrderClickListener.ViewOnMapClick(view, model, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return readyOrdersList.size();
    }

    public ArrayList<ReadyOrderModel> getSelectedOrders() {
        ArrayList<ReadyOrderModel> selectedOrders = new ArrayList<>();
        for (ReadyOrderModel readyOrderModel : readyOrdersList) {
            if (readyOrderModel.getSelectedOrder()) {
                selectedOrders.add(readyOrderModel);
            }
        }
        return selectedOrders;
    }

    public void setState(String state, String visibility) {
        this.state = state;
        this.visibility = visibility;

    }


    static class OnGoingOrdersViewHolder extends RecyclerView.ViewHolder {
        TextView txtOrderNo;
        TextView txtPaymentMode;
        TextView txtOrderPlacerName;
        TextView txtUserLocDistance;
        ImageView cashIcon;
        ImageView itemUnChecked;
        ImageView itemChecked;
        RelativeLayout onGoingOrderLayout;
        LinearLayout viewDistanceOnMap;

        public OnGoingOrdersViewHolder(@NonNull View itemView) {
            super(itemView);
            viewDistanceOnMap = itemView.findViewById(R.id.viewDistanceOnMap);
            onGoingOrderLayout = itemView.findViewById(R.id.onGoingOrderLayout);
            txtOrderNo = itemView.findViewById(R.id.txtOrderNo);
            txtPaymentMode = itemView.findViewById(R.id.txtPaymentMode);
            txtOrderPlacerName = itemView.findViewById(R.id.txtOrderPlacerName);
            txtUserLocDistance = itemView.findViewById(R.id.txtUserLocDistance);
            cashIcon = itemView.findViewById(R.id.cashIcon);
            itemUnChecked = itemView.findViewById(R.id.itemUnChecked);
            itemChecked = itemView.findViewById(R.id.itemChecked);

        }


    }

}
