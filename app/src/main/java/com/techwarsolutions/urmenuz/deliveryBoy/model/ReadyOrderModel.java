package com.techwarsolutions.urmenuz.deliveryBoy.model;

public class ReadyOrderModel {


    String txtOrderID;
    String txtOrderNo;
    String txtOrderTotal;
    String txtOrderSubTotal;
    String txtOrderServiceFee;
    String txtOrderPlacerName;
    String txtOrderPlacerNumber;
    String txtPaymentMode;
    String txtUserLocDistance;
    String txtUserLocation;
    Boolean isSelectedOrder = false;

    public ReadyOrderModel(String txtOrderID, String txtOrderNo, String txtOrderTotal, String txtOrderSubTotal, String txtOrderServiceFee, String txtOrderPlacerName, String txtOrderPlacerNumber, String txtPaymentMode, String txtUserLocDistance, String txtUserLocation) {
        this.txtOrderID = txtOrderID;
        this.txtOrderNo = txtOrderNo;
        this.txtOrderTotal = txtOrderTotal;
        this.txtOrderSubTotal = txtOrderSubTotal;
        this.txtOrderServiceFee = txtOrderServiceFee;
        this.txtOrderPlacerName = txtOrderPlacerName;
        this.txtOrderPlacerNumber = txtOrderPlacerNumber;
        this.txtPaymentMode = txtPaymentMode;
        this.txtUserLocDistance = txtUserLocDistance;
        this.txtUserLocation = txtUserLocation;
    }

    public String getTxtOrderID() {
        return txtOrderID;
    }

    public void setTxtOrderID(String txtOrderID) {
        this.txtOrderID = txtOrderID;
    }

    public String getTxtOrderNo() {
        return txtOrderNo;
    }

    public void setTxtOrderNo(String txtOrderNo) {
        this.txtOrderNo = txtOrderNo;
    }

    public String getTxtOrderTotal() {
        return txtOrderTotal;
    }

    public void setTxtOrderTotal(String txtOrderTotal) {
        this.txtOrderTotal = txtOrderTotal;
    }

    public String getTxtOrderSubTotal() {
        return txtOrderSubTotal;
    }

    public void setTxtOrderSubTotal(String txtOrderSubTotal) {
        this.txtOrderSubTotal = txtOrderSubTotal;
    }

    public String getTxtOrderServiceFee() {
        return txtOrderServiceFee;
    }

    public void setTxtOrderServiceFee(String txtOrderServiceFee) {
        this.txtOrderServiceFee = txtOrderServiceFee;
    }

    public String getTxtOrderPlacerName() {
        return txtOrderPlacerName;
    }

    public void setTxtOrderPlacerName(String txtOrderPlacerName) {
        this.txtOrderPlacerName = txtOrderPlacerName;
    }

    public String getTxtOrderPlacerNumber() {
        return txtOrderPlacerNumber;
    }

    public void setTxtOrderPlacerNumber(String txtOrderPlacerNumber) {
        this.txtOrderPlacerNumber = txtOrderPlacerNumber;
    }

    public String getTxtPaymentMode() {
        return txtPaymentMode;
    }

    public void setTxtPaymentMode(String txtPaymentMode) {
        this.txtPaymentMode = txtPaymentMode;
    }

    public String getTxtUserLocDistance() {
        return txtUserLocDistance;
    }

    public void setTxtUserLocDistance(String txtUserLocDistance) {
        this.txtUserLocDistance = txtUserLocDistance;
    }

    public String getTxtUserLocation() {
        return txtUserLocation;
    }

    public void setTxtUserLocation(String txtUserLocation) {
        this.txtUserLocation = txtUserLocation;
    }

    public Boolean getSelectedOrder() {
        return isSelectedOrder;
    }

    public void setSelectedOrder(Boolean selectedOrder) {
        isSelectedOrder = selectedOrder;
    }
}
