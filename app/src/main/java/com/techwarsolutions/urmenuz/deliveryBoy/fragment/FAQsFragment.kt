package com.techwarsolutions.urmenuz.deliveryBoy.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import kotlinx.android.synthetic.main.fragment_tems_and_conditions.*

class FAQsFragment : Fragment() {

    private var sharedPreference: SharedPreference? = null
    var swipeListener: SwipeListener? = null


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        container?.removeAllViews()

        return inflater.inflate(R.layout.fragment_tems_and_conditions, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeListener = SwipeListener(swipeBoyFAQ, requireActivity())
        sharedPreference = SharedPreference(activity)
        if (sharedPreference?.userImage != null) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(btnOpenUserSettings!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }
        btnBacksFAQ?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
        btnOpenUserSettings?.setOnClickListener {
            setFragment(BoySettingsFragment())
        }
    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()

            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.fragmentsContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()
    }

    /*override fun onDestroyView() {
        val activity = activity as? DashboardActivity
        activity?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        setHasOptionsMenu(false)
        super.onDestroyView()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val activity = activity as? DashboardActivity
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)


        }
    }
*/

}