package com.techwarsolutions.urmenuz.deliveryBoy.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.deliveryBoy.model.OnGoingOrdersModel
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import kotlinx.android.synthetic.main.fragment_going_order_detail.txtGetOrderPlacerName
import kotlinx.android.synthetic.main.fragment_going_order_detail.txtOrderTotal
import kotlinx.android.synthetic.main.fragment_going_order_detail.txtSubTotal
import kotlinx.android.synthetic.main.fragment_ready_order_details.*

class ReadyOrderDetailsFragment : Fragment() {

    var onGoingOrdersModel: OnGoingOrdersModel? = null
    var swipeListener: SwipeListener? = null
    private var sharedPreference: SharedPreference? = null


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        container?.removeAllViews()
        return inflater.inflate(R.layout.fragment_ready_order_details, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipeListener = SwipeListener(swipeLeftReadyDetails, requireActivity())
        swipeListener = SwipeListener(swipeLeftReadyDetails2, requireActivity())

        sharedPreference = SharedPreference(activity)
        if (sharedPreference?.userImage != null) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(btnOpenUserSetting!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }

        btnBackReadyDetailsOrder?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
        btnOpenUserSetting?.setOnClickListener {
            setFragment(BoySettingsFragment())
        }
        val bundle = arguments
        if (bundle != null) {
            onGoingOrdersModel =
                Gson().fromJson(bundle.getString("onGoingOrders"), OnGoingOrdersModel::class.java)

            txtSetOrderNo?.text = "Order # " + onGoingOrdersModel?.txtOrderNo
            txtGetOrderPlacerName?.text = onGoingOrdersModel?.txtOrderPlacerName
            txtSubTotal?.text = onGoingOrdersModel?.txtOrderSubTotal + " SR"
            txtOrderTotal?.text = onGoingOrdersModel?.txtOrderTotal + " SR"


            btnMakeCall?.setOnClickListener {
                try {
                    onGoingOrdersModel?.txtOrderPlacerNumber?.let { it1 -> callUser(it1) }

                } catch (e: Exception) {
                    Toast.makeText(activity, "Unable to call", Toast.LENGTH_SHORT).show()
                }
            }

            btnViewOnMap?.setOnClickListener {
                try {
                    val gmmIntentUri =
                        Uri.parse("google.navigation:q=${onGoingOrdersModel?.txtUserLocation}")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    startActivity(mapIntent)
                } catch (e: Exception) {
                    Toast.makeText(activity, "Unable to open map.", Toast.LENGTH_LONG).show()
                }
            }


        }
    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()
            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.fragmentsContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()
    }

    private fun callUser(number: String) {
        try {
            val callIntent = Intent(Intent.ACTION_CALL)
            callIntent.data = Uri.parse("tel:$number")
            startActivity(callIntent)

        } catch (e: Exception) {
            Log.e("Error Calling", e.toString())
        }
    }
}