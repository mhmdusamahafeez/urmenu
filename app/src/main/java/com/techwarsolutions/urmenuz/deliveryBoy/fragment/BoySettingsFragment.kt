package com.techwarsolutions.urmenuz.deliveryBoy.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import kotlinx.android.synthetic.main.fragment_boy_settings.*
import java.io.IOException
import java.util.*


class BoySettingsFragment : Fragment() {
    private val PICK_IMAGE_REQUEST = 71
    var mDownloadUrl: String? = null
    private var filePath: Uri? = null
    private var sharedPreference: SharedPreference? = null
    var swipeListener: SwipeListener? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_boy_settings, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        swipeListener = SwipeListener(swipeLeftSettings, requireActivity())
        sharedPreference = SharedPreference(activity)

        if (sharedPreference?.userImage != null && (sharedPreference?.userImage)?.isNotEmpty()!!) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(setUserImage!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }

        btnBackBoySetting?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
        btnBoyFAQLayouts?.setOnClickListener {
            setFragment(FAQsFragment())
        }

        uploadImage?.setOnClickListener {
            chooseImage()
        }
        setImage?.setOnClickListener {
            chooseImage()
        }

        //edit profile
        openBoyEditProfile?.setOnClickListener {
            editBoyProfileDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(swipeLeftSettings)!!)))
            editBoyProfileDialog.visibility = View.VISIBLE
        }
        btnUpdateBoyInfo?.setOnClickListener {
            boyAccUpdatedDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(swipeLeftSettings)!!)))
            boyAccUpdatedDialog.visibility = View.VISIBLE;
            editBoyProfileDialog.visibility = View.GONE;
        }
        btnBoyAccountUpdated?.setOnClickListener {
            boyAccUpdatedDialog.visibility = View.GONE;

        }

        //Logout
        btnLogoutBoy?.setOnClickListener {
            logoutCustomBoyDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(swipeLeftSettings)!!)))
            logoutCustomBoyDialog.visibility = View.VISIBLE
        }
        btnDialogLogout?.setOnClickListener {
            logoutCustomBoyDialog.visibility = View.GONE;
        }
        logoutCustomBoyDialog?.setOnClickListener {
            logoutCustomBoyDialog.visibility = View.GONE;
        }

        // Language Dialog
        btnLanguageLayout?.setOnClickListener {
            boyLanguageDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(swipeLeftSettings)!!)))
            boyLanguageDialog.visibility = View.VISIBLE
        }
        btnCloseLanguageDialog?.setOnClickListener {
            boyLanguageDialog.visibility = View.GONE;
        }
        boyLanguageDialog?.setOnClickListener {
            boyLanguageDialog.visibility = View.GONE;
        }

        //Password Dialog
        btnPasswordLayouts?.setOnClickListener {
            boyPasswordDialog.setBackgroundDrawable(BitmapDrawable(resources,
                blur(requireActivity(), captureScreenShot(swipeLeftSettings)!!)))
            boyPasswordDialog.visibility = View.VISIBLE
        }
        btnExitPassDialog?.setOnClickListener {
            boyPasswordDialog.setVisibility(View.GONE);
        }
        boyPasswordDialog?.setOnClickListener {
            boyPasswordDialog.setVisibility(View.GONE);
        }
        btnUpdateBoyPassword?.setOnClickListener {
            val oldPass = edtOldBoyPassword.text.toString().trim()
            val newPass = edtNewBoyPassword.text.toString().trim()
            if (TextUtils.isEmpty(oldPass)) {
                Toast.makeText(activity, "Enter old password!", Toast.LENGTH_SHORT).show()
            } else if (TextUtils.isEmpty(newPass)) {
                Toast.makeText(activity, "Enter new password!", Toast.LENGTH_SHORT).show()
            } else {
                boyPasswordDialog.visibility = View.GONE;
            }

        }


    }

    fun captureScreenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_4444)
        val canvas = Canvas(bitmap)
        val backgroundDrawable = view.background
        if (backgroundDrawable != null) {
            backgroundDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.parseColor("#80000000"))
        }
        view.draw(canvas)
        return bitmap
    }

    fun blur(context: Context?, image: Bitmap): Bitmap? {
        val BITMAP_SCALE = 0.4f
        val BLUR_RADIUS = 9.5f
        val width = Math.round(image.width * BITMAP_SCALE)
        val height = Math.round(image.height * BITMAP_SCALE)
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs = RenderScript.create(context)
        val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(BLUR_RADIUS)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }


    private fun chooseImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        uploadImage?.visibility = View.GONE
        setImage?.visibility = View.VISIBLE

        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            filePath = data.data
            try {
                val bitmap =
                    MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, filePath)
                mDownloadUrl = filePath.toString()
                sharedPreference?.saveUserImage(filePath.toString())
                setImage!!.setImageBitmap(bitmap)
                setUserImage!!.setImageBitmap(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()

            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.fragmentsContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()


    }

    private fun setLocale(language: String) {
/*
        Toast.makeText(
            activity, "Received = $language", Toast.LENGTH_SHORT
        ).show()*/
        val resource = resources
        val metrics = resource.displayMetrics
        val configuration = resource.configuration
        val locale = Locale(language)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale)
        } else {
            configuration.locale = locale
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) activity?.createConfigurationContext(
            configuration)
        resource.updateConfiguration(configuration, metrics)

        onConfigurationChanged(configuration)

        //sharedPreference?.saveSelectedLanguage(language)

    }


}