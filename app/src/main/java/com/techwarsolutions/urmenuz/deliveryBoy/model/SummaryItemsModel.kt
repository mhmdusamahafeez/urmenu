package com.techwarsolutions.urmenuz.deliveryBoy.model

class SummaryItemsModel(
    var orderID: String?,
    var itemID: String?,
    var itemName: String?,
    var itemQuantity: String?,
    var eachItemPrice: String?
)