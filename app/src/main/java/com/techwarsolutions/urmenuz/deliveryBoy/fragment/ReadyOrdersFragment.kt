package com.techwarsolutions.urmenuz.deliveryBoy.fragment

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.deliveryBoy.adapter.ReadyOrdersAdapter
import com.techwarsolutions.urmenuz.deliveryBoy.interfaces.ReadyOrdersInterface
import com.techwarsolutions.urmenuz.deliveryBoy.model.ReadyOrderModel
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.utils.SwipeListener
import kotlinx.android.synthetic.main.fragment_ready_orders.*


class ReadyOrdersFragment : Fragment(), ReadyOrdersInterface.OnReadyOrderClickListener {

    var swipeListener: SwipeListener? = null
    var readyOrderModel: ReadyOrderModel? = null
    private var onGoingArrayList: ArrayList<ReadyOrderModel>? = null
    var readyOrdersAdapter: ReadyOrdersAdapter? = null
    var sharedPreference: SharedPreference? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        container?.removeAllViews()
        return inflater.inflate(R.layout.fragment_ready_orders, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipeListener = SwipeListener(swipeLeftReady, requireActivity())
        sharedPreference = SharedPreference(activity)
        if (sharedPreference?.userImage != null) {
            try {
                Glide.with(requireActivity()).load(sharedPreference?.userImage)
                    .placeholder(R.drawable.user).into(btnOpenUserSettings!!)
            } catch (e: Exception) {
                Log.e("ImageError", e.toString())
            }
        }


        btnOpenUserSettings?.setOnClickListener {
            setFragment(BoySettingsFragment())
        }

        btnBackReadyOrder?.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }

        onGoingArrayList = ArrayList()
        onGoingArrayList?.add(ReadyOrderModel("1",
            "537",
            "20.00",
            "10.00",
            "21.00",
            "Usama Hafeez",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(ReadyOrderModel("2",
            "538",
            "220.00",
            "10.00",
            "230.00",
            "Muhammad Ali",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(ReadyOrderModel("3",
            "539",
            "250.00",
            "10.00",
            "260.00",
            "Muhammad Abdullah",
            "03478302658",
            "Cash",
            "10",
            "170/B Muhafiz Town, Sargodha"

        ))
        onGoingArrayList?.add(ReadyOrderModel("1",
            "540",
            "20.00",
            "10.00",
            "21.00",
            "Ali Raza",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(ReadyOrderModel("2",
            "541",
            "220.00",
            "10.00",
            "230.00",
            "Jabran Yousaf",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(ReadyOrderModel("1",
            "540",
            "20.00",
            "10.00",
            "21.00",
            "Ali Raza",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(ReadyOrderModel("2",
            "541",
            "220.00",
            "10.00",
            "230.00",
            "Jabran Yousaf",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))
        onGoingArrayList?.add(ReadyOrderModel("1",
            "540",
            "20.00",
            "10.00",
            "21.00",
            "Ali Raza",
            "03065015216",
            "Paid",
            "20",
            "168/B Muhafiz Town, Sargodha"))

        onGoingArrayList?.add(ReadyOrderModel("2",
            "541",
            "220.00",
            "10.00",
            "230.00",
            "Jabran Yousaf",
            "03478302658",
            "Cash",
            "10",
            "169/B Muhafiz Town, Sargodha"))


        if (onGoingArrayList != null) {
            readyOrdersAdapter = ReadyOrdersAdapter(activity, onGoingArrayList, this)
            rvOnGoingOrders?.adapter = readyOrdersAdapter

        } else {
            txtNoOnGoingOrder?.visibility = View.VISIBLE
        }

        btnSelectOrders?.setOnClickListener {
            readyOrderModel?.selectedOrder = true
            readyOrdersAdapter?.setState("true", "VISIBLE")
            readyOrdersAdapter?.notifyDataSetChanged()
            btnAddOutgoing?.visibility = View.VISIBLE
            btnCancelSelectOrders?.visibility = View.VISIBLE
            btnSelectOrders?.visibility = View.GONE

        }
        btnCancelSelectOrders?.setOnClickListener {
            readyOrdersAdapter?.setState("false", "GONE")
            readyOrdersAdapter?.notifyDataSetChanged()
            btnAddOutgoing.visibility = View.GONE
            btnCancelSelectOrders?.visibility = View.GONE
            btnSelectOrders?.visibility = View.VISIBLE

        }

        //Must Select Items
        mustSelectOrderDialog?.setOnClickListener {
            mustSelectOrderDialog?.visibility = View.GONE
        }
        btnOkSelectItem.setOnClickListener {
            mustSelectOrderDialog?.visibility = View.GONE
        }

        // Outgoing Orders
        btnAddOutgoing?.setOnClickListener {
            val selectedItem: ArrayList<ReadyOrderModel> = readyOrdersAdapter?.selectedOrders!!
            if (selectedItem.isEmpty()) {

                mustSelectOrderDialog.setBackgroundDrawable(BitmapDrawable(resources,
                    blur(requireActivity(), captureScreenShot(swipeLeftReady)!!)))
                mustSelectOrderDialog.visibility = View.VISIBLE

                /*    val inflater = requireActivity().layoutInflater
                    val view = inflater.inflate(R.layout.must_select_dialog, null)
                    val infoDialogBuilder = AlertDialog.Builder(activity)

                    val btnOkSelectItem: TextView = view.findViewById(R.id.btnOkSelectItem)

                    infoDialogBuilder.setView(view)
                    val infoDialog = infoDialogBuilder.create()

                    infoDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
                    btnOkSelectItem.setOnClickListener {
                        infoDialog?.dismiss()
                    }
                    infoDialog.setContentView(view)
                    infoDialog.show()*/
            } else {
                val orderNo = java.lang.StringBuilder()
                for (i in selectedItem.indices) {
                    if (i == 0) {
                        orderNo.append(selectedItem[i].txtOrderNo)
                    } else {
                        orderNo.append("\n").append(selectedItem[i].txtOrderNo)
                    }
                }
                if (activity?.supportFragmentManager?.backStackEntryCount!! > 1) {
                    activity?.supportFragmentManager?.popBackStack()
                }

                Toast.makeText(activity, "Successfully added to outgoing", Toast.LENGTH_SHORT)
                    .show()
            }


        }
    }

    override fun onSelectOrderAction(isSelected: Boolean?) {
        if (isSelected == true) {
            btnAddOutgoing?.visibility = View.VISIBLE
            readyOrdersAdapter?.notifyDataSetChanged()
        } else {
            btnAddOutgoing?.visibility = View.GONE
            readyOrdersAdapter?.notifyDataSetChanged()

        }
    }

    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()

            ?.setCustomAnimations(0, 0, R.anim.enter_from_left, R.anim.exit_to_right)
            ?.replace(R.id.fragmentsContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)?.commit()


    }

    override fun OnReadyOrderClick(view: View?, readyOrderModel: ReadyOrderModel?, position: Int) {
        val bundle = Bundle()
        bundle.putString("onGoingOrders", Gson().toJson(readyOrderModel))
        val nextFrag = ReadyOrderDetailsFragment()
        nextFrag.arguments = bundle
        setFragment(nextFrag)
    }

    override fun ViewOnMapClick(view: View?, readyOrderModel: ReadyOrderModel?, position: Int) {
        try {
            val gmmIntentUri = Uri.parse("google.navigation:q=${readyOrderModel?.txtUserLocation}")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        } catch (e: Exception) {
            Toast.makeText(activity, "Unable to open map.", Toast.LENGTH_LONG).show()
        }
    }

    fun captureScreenShot(view: View): Bitmap? {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_4444)
        val canvas = Canvas(bitmap)
        val backgroundDrawable = view.background
        if (backgroundDrawable != null) {
            backgroundDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.parseColor("#80000000"))
        }
        view.draw(canvas)
        return bitmap
    }

    fun blur(context: Context?, image: Bitmap): Bitmap? {
        val BITMAP_SCALE = 0.4f
        val BLUR_RADIUS = 9.5f
        val width = Math.round(image.width * BITMAP_SCALE)
        val height = Math.round(image.height * BITMAP_SCALE)
        val inputBitmap = Bitmap.createScaledBitmap(image, width, height, false)
        val outputBitmap = Bitmap.createBitmap(inputBitmap)
        val rs = RenderScript.create(context)
        val theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs))
        val tmpIn = Allocation.createFromBitmap(rs, inputBitmap)
        val tmpOut = Allocation.createFromBitmap(rs, outputBitmap)
        theIntrinsic.setRadius(BLUR_RADIUS)
        theIntrinsic.setInput(tmpIn)
        theIntrinsic.forEach(tmpOut)
        tmpOut.copyTo(outputBitmap)
        return outputBitmap
    }


}