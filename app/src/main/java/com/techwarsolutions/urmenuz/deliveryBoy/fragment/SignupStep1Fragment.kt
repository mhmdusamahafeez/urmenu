package com.techwarsolutions.urmenuz.deliveryBoy.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.techwarsolutions.urmenuz.LoginActivity
import com.techwarsolutions.urmenuz.R
import com.techwarsolutions.urmenuz.utils.SharedPreference
import kotlinx.android.synthetic.main.fragment_signup_step1.*


class SignupStep1Fragment : Fragment() {

    private var sharedPreference: SharedPreference? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_signup_step1, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sharedPreference = SharedPreference(activity)
        alreadyAccount?.setOnClickListener {
            val intent =
                Intent(activity, LoginActivity::class.java)
            startActivity(intent)
        }

        btnSignUpNext?.setOnClickListener {
            pbSearchEmail.visibility = View.VISIBLE
            val email = edtGetEmailAddress.text.toString().trim()
            if(TextUtils.isEmpty(email)){
                pbSearchEmail.visibility = View.GONE
                edtGetEmailAddress.error = "Please enter email address"
                Toast.makeText(activity, "Please enter email address", Toast.LENGTH_SHORT).show()


            }else if(email != "waiter@gmail.com"){
                pbSearchEmail.visibility = View.GONE
                Toast.makeText(activity, "Invalid EmailAddress", Toast.LENGTH_SHORT).show()

            }else{
                Handler().postDelayed({
                    setFragment(SignupStep2Fragment())
                    sharedPreference!!.saveUserEmail(email)

                }, 500)


            }

        }


    }

    fun showErrorSnackBar(msg: String) {
        try {
            val snackbar =
                Snackbar.make(requireActivity().findViewById(android.R.id.content), msg, 2500)
            snackbar.setActionTextColor(resources.getColor(R.color.white))
            snackbar.view.setBackgroundColor(resources.getColor(R.color.light_brown))
            snackbar.show()
        } catch (e: Exception) {
            Log.e("SnackBarError", e.toString())
        }

    }


    private fun setFragment(fragment: Fragment?) {
        activity?.supportFragmentManager?.beginTransaction()

            ?.setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            ?.replace(R.id.signUpContainer, fragment!!)
            ?.addToBackStack(fragment.javaClass.canonicalName)
            ?.commit()


    }

}