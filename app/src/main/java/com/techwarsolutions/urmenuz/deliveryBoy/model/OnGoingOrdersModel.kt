package com.techwarsolutions.urmenuz.deliveryBoy.model

class OnGoingOrdersModel(
    var txtOrderID: String?,
    var txtOrderNo: String?,
    var txtOrderSubTotal: String?,
    var txtOrderServiceFee: String?,
    var txtOrderTotal: String?,
    var txtOrderPlacerName: String?,
    var txtOrderPlacerNumber: String?,
    var txtPaymentMode: String?,
    var txtUserLocDistance: String?,
    var txtUserLocation: String?


)