package com.techwarsolutions.urmenuz.Database;

import android.content.Context;
import androidx.room.Room;


public class DatabaseClient {

    private Context context;
    private static DatabaseClient mIstance;

    private urMenuDatabase urMenuDatabase;

    private DatabaseClient(Context context){
        this.context=context;

        urMenuDatabase = Room.databaseBuilder(context, urMenuDatabase.class, "urMenuDB").allowMainThreadQueries().build();
    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mIstance == null) {
            mIstance = new DatabaseClient(mCtx);
        }
        return mIstance;
    }

    public urMenuDatabase getUserDatabase() {
        return urMenuDatabase;
    }
}
