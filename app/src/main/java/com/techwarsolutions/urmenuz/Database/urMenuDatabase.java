package com.techwarsolutions.urmenuz.Database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.techwarsolutions.urmenuz.DAO.MenuDAO;
import com.techwarsolutions.urmenuz.DatabaseModels.MenuModel;


@Database(entities = {MenuModel.class},version = 1,exportSchema = false)
public abstract class urMenuDatabase extends RoomDatabase {
    public abstract MenuDAO cartDAO();
}
