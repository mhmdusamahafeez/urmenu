package com.techwarsolutions.urmenuz.utils

import android.app.Activity
import android.util.Log
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.techwarsolutions.urmenuz.R


class SwipeListener internal constructor(view: View, activity: Activity) :
    OnTouchListener {
    var detector: GestureDetector
    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        return detector.onTouchEvent(motionEvent)
    }

    init {
        val threshold = 100
        val velocity_threshold = 100
        val listener: SimpleOnGestureListener = object : SimpleOnGestureListener() {
            override fun onDown(e: MotionEvent): Boolean {
                return true
            }

            override fun onFling(
                e1: MotionEvent?,
                e2: MotionEvent?,
                velocityX: Float,
                velocityY: Float
            ): Boolean {
                try {
                val xDiff = e2?.x!! - e1?.x!!
                val yDiff = e2?.y!! - e1?.y!!

                    if (Math.abs(xDiff) > Math.abs(yDiff)) {
                        if (Math.abs(xDiff) > threshold &&
                            Math.abs(velocityX) > velocity_threshold
                        ) {
                            if (xDiff > 0) {
                                activity?.onBackPressed()
                                //Swipe Left
                            } else {
                                //Swipe Right
                                Log.e("SwipeInfo", "Swiped Right")
                            }
                            return true
                        }
                    } else {

                        if(Math.abs(yDiff) > threshold &&
                            Math.abs(velocityY) > velocity_threshold){
                            if(yDiff > 0){
                                Log.e("SwipeInfo", "Swiped Down")
                            }else{
                                Log.e("SwipeInfo", "Swiped Up")

                            }

                            return true

                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                return false
            }
        }
        detector = GestureDetector(listener)
        view.setOnTouchListener(this)
    }
}

