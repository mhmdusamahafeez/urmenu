package com.techwarsolutions.urmenuz.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {

    public static final String PREF_NAME = "urMenu";
    public static final String KEY_LanguageSelected = "language";
    public static final String KEY_UserName = "user_name";
    public static final String KEY_UserEmail = "user_email";
    public static final String KEY_WaiterEmail = "waiter_email";
    public static final String KEY_UserPhoneNumber = "user_no";
    public static final String KEY_UserFullPhoneNumber = "user_fno";
    public static final String KEY_UserPassword = "user_password";
    public static final String KEY_PasswordCreated = "password_created";
    public static final String KEY_LodgedIn = "login";
    public static final String KEY_AddedToTab = "AddedToTab";
    public static final String KEY_ImageURL = "userImage";


    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public SharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = sharedPreferences.edit();
        editor.apply();
    }

    public String getSelectedLanguage() {
        return sharedPreferences.getString(KEY_LanguageSelected, "");
    }

    public void saveSelectedLanguage(String language) {
        editor.putString(KEY_LanguageSelected, language);
        editor.commit();
    }

    public String getUserName() {
        return sharedPreferences.getString(KEY_UserName, "");
    }

    public void saveUserName(String name) {
        editor.putString(KEY_UserName, name);
        editor.commit();
    }

    public String getUserImage() {
        return sharedPreferences.getString(KEY_ImageURL, "");
    }

    public void saveUserImage(String url) {
        editor.putString(KEY_ImageURL, url);
        editor.commit();
    }

    public String getUserEmail() {
        return sharedPreferences.getString(KEY_UserEmail, "");
    }

    public void saveUserEmail(String email) {
        editor.putString(KEY_UserEmail, email);
        editor.commit();
    }

    public String getWaiterEmail() {
        return sharedPreferences.getString(KEY_WaiterEmail, "");
    }

    public void saveWaiterEmail(String email) {
        editor.putString(KEY_WaiterEmail, email);
        editor.commit();
    }

    public String getUserPhNo() {
        return sharedPreferences.getString(KEY_UserPhoneNumber, "");
    }

    public void saveUserPhNo(String no) {
        editor.putString(KEY_UserPhoneNumber, no);
        editor.commit();
    }

    public String getUserFullPhNo() {
        return sharedPreferences.getString(KEY_UserFullPhoneNumber, "");
    }

    public void saveUserFullPhNo(String no) {
        editor.putString(KEY_UserFullPhoneNumber, no);
        editor.commit();
    }

    public String getUserPassword() {
        return sharedPreferences.getString(KEY_UserPassword, "");
    }

    public void saveUserPassword(String password) {
        editor.putString(KEY_UserPassword, password);
        editor.commit();
    }

    public String isPasswordCreated() {
        return sharedPreferences.getString(KEY_PasswordCreated, "");
    }

    public void savePassCreated(String password) {
        editor.putString(KEY_PasswordCreated, password);
        editor.commit();
    }

    public String isLogin() {
        return sharedPreferences.getString(KEY_LodgedIn, "");
    }

    public void saveLoginStatus(String password) {
        editor.putString(KEY_LodgedIn, password);
        editor.commit();
    }

    public String getAddToTab() {
        return sharedPreferences.getString(KEY_AddedToTab, "");
    }

    public void saveAddToTab(String tabStatus) {
        editor.putString(KEY_AddedToTab, tabStatus);
        editor.commit();
    }


}
