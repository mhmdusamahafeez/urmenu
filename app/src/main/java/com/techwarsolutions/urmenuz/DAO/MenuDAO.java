package com.techwarsolutions.urmenuz.DAO;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.techwarsolutions.urmenuz.DatabaseModels.MenuModel;

import java.util.List;

@Dao
public interface MenuDAO {

    @Query("Select * from MenuModel")
    List<MenuModel> getAll();

    @Query("Delete FROM MenuModel")
    void deleteAll();

    @Query("SELECT COUNT(*) FROM MenuModel")
    int totalCartItems();


    @Query("SELECT EXISTS(SELECT * FROM MenuModel where ItemID == :ItemID)")
    boolean isAlreadyExist(String ItemID);


    @Query("SELECT ItemQuantity FROM MenuModel where ItemID == :ItemID")
    Double fetchItemQuantity(String ItemID);


    @Query("UPDATE MenuModel SET ItemQuantity=:quantity WHERE ItemID = :ItemID")
    void updateQuantity(Double quantity, String ItemID);


    @Query("Delete FROM MenuModel where ItemID == :ItemID")
    void deleteItem(String ItemID);

    @Insert
    void insert(MenuModel MenuModel);

    @Delete
    void delete(MenuModel MenuModel);

    @Update
    void update(MenuModel MenuModel);
}
