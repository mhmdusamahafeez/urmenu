package com.techwarsolutions.urmenuz

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.techwarsolutions.urmenuz.deliveryBoy.BoyDashboardActivity
import com.techwarsolutions.urmenuz.utils.SharedPreference
import com.techwarsolutions.urmenuz.waiter.WaiterDashboardActivity
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private var sharedPreference: SharedPreference? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        sharedPreference = SharedPreference(this@LoginActivity)

        mylist.add(Manifest.permission.ACCESS_FINE_LOCATION);
        mylist.add(Manifest.permission.CALL_PHONE);
        mylist.add(Manifest.permission.INTERNET);
        checkPermission(this@LoginActivity, mylist);

        edtEmail.setText("waiter@gmail.com")
        edtPassword.setText("123456")
        hidePassword?.setOnClickListener {
            showPassword?.visibility = View.VISIBLE
            hidePassword?.visibility = View.GONE

            edtPassword.transformationMethod = PasswordTransformationMethod.getInstance()
        }
        showPassword?.setOnClickListener {
            showPassword?.visibility = View.GONE
            hidePassword?.visibility = View.VISIBLE
            edtPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
        }


        btnLoginUser?.setOnClickListener {
            try {
                pbLogin.visibility = View.VISIBLE
                val email = edtEmail.text.toString().trim()
                val password = edtPassword.text.toString().trim()
                if (TextUtils.isEmpty(email)) {
                    pbLogin.visibility = View.GONE

                    edtEmail.error = "Please Enter Your Email"
                    showErrorSnackBar("Please Enter Your Email")


                } else if (TextUtils.isEmpty(password)) {
                    pbLogin.visibility = View.GONE
                    edtPassword.error = "Please Enter Your Password"
                    showErrorSnackBar("Please Enter Your Password")
                }/* else if (!chkBoxTerms.isChecked) {
                showErrorSnackBar("Agree with our terms and condition")
            }*/ else {
                    loginUserAccount(email, password)
                }
            } catch (e: Exception) {
                Log.e("LoginBTNClick", e.toString())

            }

        }

        btnCreateAccount?.setOnClickListener {
            val intent =
                Intent(this@LoginActivity, PreLoginActivity::class.java)
            startActivity(intent)
        }

    }
    override fun onBackPressed() {
            finishAffinity()
    }

    private fun loginUserAccount(txtEmail: String, txtPassword: String) {
        try {

           /* val intent =
                Intent(this@LoginActivity, BoyDashboardActivity::class.java)
            startActivity(intent)*/

            if (txtEmail != sharedPreference?.userEmail) {
                Toast.makeText(applicationContext, "Invalid Email", Toast.LENGTH_SHORT).show()
                pbLogin.visibility = View.GONE

            } else if (txtPassword != sharedPreference?.userPassword) {
                Toast.makeText(applicationContext, "Invalid Password", Toast.LENGTH_SHORT).show()
                pbLogin.visibility = View.GONE

            } else {

                pbLogin.visibility = View.VISIBLE
                sharedPreference?.saveLoginStatus("true")
                Toast.makeText(applicationContext, "Login Successfully", Toast.LENGTH_SHORT).show()

                //val intent = Intent(this@LoginActivity, BoyDashboardActivity::class.java)
                val intent = Intent(this@LoginActivity, WaiterDashboardActivity::class.java)
                startActivity(intent)
            }


        } catch (e: Exception) {
            Log.e("LoginFunError", e.toString())
        }
    }

    fun showErrorSnackBar(msg: String) {
        try {
            val snackbar = Snackbar.make(findViewById(android.R.id.content), msg, 2500)
            snackbar.setActionTextColor(resources.getColor(R.color.white))
            snackbar.view.setBackgroundColor(resources.getColor(R.color.light_brown))
            snackbar.show()
        } catch (e: Exception) {
            Log.e("SnackBarError", e.toString())
        }

    }
    var mylist: ArrayList<String> = ArrayList()
    private fun checkPermission(mainActivity: LoginActivity, permission: List<String>) {
        try {
            for (i in permission.indices) {
                if (ContextCompat.checkSelfPermission(
                        mainActivity,
                        permission[i]
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    Log.i("Permission: ", "Granted")
                } else {
                    Log.i("Permission: ", "Denied")
                }
            }
            if (!permission.isEmpty()) {
                ActivityCompat.requestPermissions(mainActivity, permission.toTypedArray(), 101)
            }
        } catch (e: Exception) {
            Log.e("PermissionFunError", e.toString())

        }

    }

}